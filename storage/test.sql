/*
Ermittle
- alle Kontakte, die die DOI-Mail nicht bestätigt haben
- alle Kontakte, die sich abgemeldet haben
jeweils nach 6 Monaten


Beispiele:
Wir haben eCommerce und Retail als jeweiligen SubName in Subscriptions (es gibt quasi Subscription-Typen mit dem Feld SubName)

Wenn eCommerce DOI nicht bestaetigt, darf auch Retail nicht bestaetigt sein UND  das InsertTimeStamp bei Retail muss aelter als X sein
Wenn eCommerce unsubscribed UND dessen Unsubscribe-Date aelter X, darf Retail keinen unsubscribed haben der jeunger als X ist

Er darf NIE ein Subscription_Active haben (einfachste Ueberlegung, denn dann ist er ja aktiv und darf nicht entfernt werden)

Es kann re-subscribes geben, so dass nach einer SubVersion X mit unsubscribed eine SubVersion X + 1 folgen kann mit subscribed

*/
Select c.ID as ContactID From Contacts c
/* es darf keine active subscription geben */
Where Not Exists (
    Select suac.ContactID From Subscription_Actives suac
    Where suac.ContactID = c.ID
)
AND (
    /*  es darf keinen Eintrag in Subscriptions_Pub innerhalb der letzten 6 monate vom typ requested geben, bei denen es hoehere versionen gibt (== nur requested, aber nie ein subscribe)
        also suchen wir nach dem Vorkommen -> wenn gefunden darf geloescht werden
    */
    Exists (
        Select su.ContactID
        From Subscriptions su
        Where su.ContactID = c.ID
        AND su.InsertTimestamp < DateAdd( month, -6, GetDate())
        AND su.SubTextStatus = 'requested'
        AND Not Exists (
            Select su3.ContactID
            From Subscriptions su3
            Where su3.ContactID = su.ContactID
            And su3.SubVersion > su.SubVersion
            And su3.SubChannel = su.SubChannel
        )
        AND Not Exists (
            Select su4.ContactID
            From Subscriptions su4
            Where su4.ContactID = su.ContactID
            AND su4.InsertTimestamp > DateAdd( month, -6, GetDate())
            And su4.SubChannel = su.SubChannel
        )
    )
    /* alle unsubscribed aelter als X, die nicht zeitgleich einen anderen unsubscribed haben juenger als x */
    OR Exists (
        Select unsub.ContactID
        From Subscriptions unsub
        Where unsub.ContactID = c.ID
        AND unsub.SubStatus = 0
        AND unsub.UnsubDate < DateAdd( month, -6, GetDate())
        /* es darf keine hoehere SubVersion in diesem scope geben, bspw nach einem re-subscribe */
        AND Not Exists (
            Select su3.ContactID
            From Subscriptions su3
            Where su3.ContactID = unsub.ContactID
            And su3.SubVersion > unsub.SubVersion
            And su3.SubName = unsub.SubName
            And su3.SubChannel = unsub.SubChannel
        )
        /* es darf keine anderen unsubcribed geben, die noch juenger als x sind */
        And Not Exists (
            Select unsub2.ContactID
            From Subscriptions unsub2
            Where unsub2.ContactID = c.ID
            AND unsub2.SubStatus = 0
            AND unsub2.UnsubDate > DateAdd( month, -6, GetDate())
            AND unsub2.SubName != unsub.SubName
        )
    )
)
