<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Company;
use Faker\Generator as Faker;

$factory->define(Company::class, function (Faker $faker) {
    return [
        'established_at' => $faker->dateTimeThisCentury,
        'name' => $faker->company,
        'staff_count' => $faker->numberBetween(2, 56),
        'description' => $faker->paragraph(3),
        'challenges' => $faker->paragraph(3),
        'strengths' => $faker->paragraph(3)
    ];
});
