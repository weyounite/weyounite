<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Address;
use Faker\Generator as Faker;

$factory->define(Address::class, function (Faker $faker) {
    return [
        'street' => $faker->streetName,
        'street_number' => $faker->streetSuffix,
        'zip' => $faker->postcode,
        'city' => $faker->city,
        'lat' => $faker->latitude,
        'lng' => $faker->longitude
    ];
});
