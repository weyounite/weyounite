<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProjectsSupportedTableSeeder extends Seeder
{
    public function run()
    {

        DB::table('projectsSupported')->insert([
            [
                'id' => 1,
                'user_id' => 4,
                'project_id' => 1,
            ],
            [
                'id' => 2,
                'user_id' => 5,
                'project_id' => 2,
            ],
        ]);
    }
}
