<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProjectsTableSeeder extends Seeder
{
    public function run()
    {

        DB::table('projects')->insert([
            [
                'id' => 1,
                'title' => 'Fit & Strong',
                'description' => 'Mein Studio ist mein Lebenstraum – jeder Kurs ist extra aufgesetzt, ich arbeite jede Woche dran, den Teilnhmern neue Ideen, Inhalt und Übungen zu geben und den Spaß in den Vordergrund zu stellen, den Teamspirit beim gemeinsamen trainieren. Kleine Kurse, immer im Kontakt und jede Woche etwas besser – das will ich jedem bieten können! Mein Studio habe ich schon einen Tag vor der Anordnung geschlossen – dass meine Teilnehmer gesund bleiben war mir extrem wichtig. Das nimmt nicht nur dem Studio die Einnahmen, es nimmt auch meinen Teilnehmern die Chance, gemeinsam an sich selbst zu arbeiten. Beides zusammen ist für mich die größte Herausfordeurng. Natürlich helfen mir Spenden, das Studio über die Zeit zu retten! Aber noch wichtiger für mich und alle, die mit mir trainieren: Helft mir, die Kurse in jede Wohnung zu bringen! Egal ob Hilfe beim Filmen, bei der Bereitstellung der Inhalte oder beim Aufbau des online Service. Mit eurer Hilfe bleibt Fit&Strong eine starke Gemeinschaft!',
                'lat' => 50.933185,
                'lng' => 6.946685,
                'contact' => 'fit-strong@email.de',
            ],
            [
                'id' => 2,
                'title' => 'Hair by Paola',
                'description' => 'Wir sind ein kleines Team von Friseuren mit einer gemeinsamen Leidenschaft – das Beste aus den Haaren jedes unserer Kunden zu holen! Gemeinsam kreieren wir nicht nur tolle Haarmode sondern vor allem auch ein Ambiente, in dem sich unsere Kunden wohlfühlen und entspannt die Zeit bei uns genießen können. Erst wurden die Kunden weniger, dann mussten wir unseren Salon schließen. Die aktuelle Situation führt dazu, dass wir unserer Leidenschaft nicht mehr nachgehen können. Di Rechnungen aber bleiben. Ein solches Team kann ich nicht einfach ersetzen – deshalb setzen wir auf die Hilfe jedes Einzelnen, um das, was wir gemeinsam aufgebaut haben, zu retten. Helft uns – durch den Kauf eines Gutscheins, eine Spende oder indem ihr andere dazu ermutigt, genau das zu tun! Wir wollen nicht reich werden, sondern das was wir aufgebaut haben durch diese Zeit retten! Da wir in dieser Zeit auch einiges reparieren, ist jede helfende Hand auch gerne willkommen!',
                'lat' => 50.935568,
                'lng' => 6.920189,
                'contact' => '+49/17610010010',
            ],
            [
                'id' => 3,
                'title' => 'Santos',
                'description' => 'Carina und ich teilen die Leidenschaft für guten Wein, noch besseres Essen und das Gefühl abends im Innenhof eines italienischen Restaurants den Tag ausklingen zu lassen. Und genau diese Leidenschaft soll jeder Gast bei uns spüren. So viele frische leckere Zutaten wie möglich, zwei Köche mit Liebe für den perfekten Geschmack und italienische Musik. Und Gäste, die immer wieder gerne mit uns in das Gefühl des letzten Urlaubs tauchen. Wir können aktuell zwar weiter Essen an unsere Kunden liefern – aber das Gefühl von Bella Italia fehlt. Für uns und die Miete können wir damit sorgen, aber wir haben einfach nicht die Arbeit für unser gesamtes Team. Und jeder einzelne und seine Familie gehören zu unserer Famiglia. Ihr könnt uns unterstützen, das Bella Italia Gefühl nach dieser Zeit wieder zu euch zu bringen! Mit jedem Gutschein und jeder Spende kommen wir diesem Ziel einen Schritt näher. Und wenn ihr Ideen habt, wie wir in dieser Zeit online oder Offline uns verbessern können – jeder ist herzlich eingeladen, Teil der Famiglia Santos zu werden!',
                'lat' => 50.925658,
                'lng' => 6.955980,
                'contact' => 'santos@email.de',
            ],
        ]);
    }
}
