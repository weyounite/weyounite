<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        $helpersUser = factory(\App\User::class, 10)->create();

        $helpersUser->each(function($user) {
            $user->person()->save(factory(\App\Person::class)->make());
            $user->helper()->save(factory(\App\Helper::class)->make());

            $user->helper->address()->save(factory(\App\Address::class)->make());
        });

        $companiesUser = factory(\App\User::class, 10)->create();

        $companiesUser->each(function($user) {
            $user->person()->save(factory(\App\Person::class)->make());
            $user->company()->save(factory(\App\Company::class)->make());


            $user->company->address()->save(factory(\App\Address::class)->make());
        });

        $projects = factory(\App\Project::class, 10)->create();

        $projects->each(function($project) use($helpersUser, $companiesUser) {
            \App\ProjectsOwned::create(['user_id' => $companiesUser->shift()->id, 'project_id' => $project->id]);
            \App\ProjectsSupported::create(['user_id' => $helpersUser->shift()->id, 'project_id' => $project->id]);
        });
    }
}
