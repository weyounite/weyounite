<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProjectsOwnedTableSeeder extends Seeder
{
    public function run()
    {

        DB::table('projectsOwned')->insert([
            [
                'id' => 1,
                'user_id' => 1,
                'project_id' => 2,
            ],
            [
                'id' => 2,
                'user_id' => 2,
                'project_id' => 1,
            ],
            [
                'id' => 3,
                'user_id' => 3,
                'project_id' => 3,
            ],
        ]);
    }
}
