<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/** a public api part */
Route::post('/contact/request', 'API\ContactResourceController@store');

Route::post("/projects/search", 'API\ProjectResourceController@search');

/** the private part */
Route::post('/auth/token', 'API\AuthController@auth');

Route::middleware('auth:sanctum')->group(function() {
    Route::get('/user', function (Request $request) {
        return $request->user();
    });

    Route::resource('projects', 'API\ProjectResourceController');

    Route::post("/projects/upload/{project}", 'API\ProjectResourceController@upload');

    Route::resource('projects_supported', 'API\ProjectsSupportedResourceController');

    Route::resource('users', 'API\UserResourceController')->except([
        'store', 'update'
    ]);

    Route::post("/users/upload/{user}", 'API\UserResourceController@upload');

    Route::get('messages', 'API\DirectMessageController@get');
    Route::get('messages/{message}', 'API\DirectMessageController@show');
    Route::post('messages/send/{receiver}', 'API\DirectMessageController@sendMessage');
    Route::put('messages/mark_as_read/{message}', 'API\DirectMessageController@markAsUnread');

    Route::resource('companies', 'API\CompanyResourceController')->except(['store', 'index', 'show']);
    Route::post('companies/{user}', 'API\CompanyResourceController@store');

    Route::resource('helper', 'API\HelperResourceController')->except(['store', 'index', 'show']);
    Route::post('helper/{user}', 'API\HelperResourceController@store');
});



