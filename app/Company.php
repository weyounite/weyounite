<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphOne;

/**
 * Class Company
 * @package App
 */
class Company extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['name', 'established_at', 'user_id', 'staff_count', 'description', 'challenges', 'strengths'];

    /**
     * @var array
     */
    protected $dates = [
        'created_at', 'updated_at', 'deleted_at', 'established_at'
    ];

    /**
     * @var array
     */
    protected $casts = [
        'staff_count' => 'integer'
    ];

    /**
     * @return MorphOne
     */
    public function address(): MorphOne
    {
        return $this->morphOne(Address::class, 'addressable');
    }

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
