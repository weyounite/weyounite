<?php

namespace App\Http\Resources;

use App\Person;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;

class UserResource extends JsonResource
{
    private $__resource;

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $this->__resource = [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'person' => PersonResource::make($this->person),
            'helper' => HelperResource::make($this->helper),
            'company' => CompanyResource::make($this->company),
            'projectsOwned' => ProjectResource::collection($this->whenLoaded('projectsOwned')),
            'projectsSupported' => ProjectResource::collection($this->whenLoaded('projectsSupported')),
            'files' => $this->getFileURLs()
        ];

        return $this->__resource;
    }

    /**
     * @return Collection
     */
    public function getFileURLs(): Collection
    {
        return $this->getMedia('user-files')->map(function($media) {
            return $media->getURL();
        });
    }
}
