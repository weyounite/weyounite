<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Arr;

class DirectMessageResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => [
                'id' => $this->id,
                'senderId' => Arr::get($this->data, 'senderId'),
                'replyToId' => Arr::get($this->data, 'replyToId'),
                'message' => Arr::get($this->data, 'message'),
                'created_at' => $this->created_at,
                'read_at' => $this->read_at
            ]
        ];
    }
}
