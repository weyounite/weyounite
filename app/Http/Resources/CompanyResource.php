<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CompanyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'established_at' => $this->established_at,
            'name' => $this->name,
            'staff_count' => $this->staff_count,
            'description' => $this->description,
            'challenges' => $this->challenges,
            'strengths' => $this->strengths,
            'address' => AddressResource::make($this->address)
        ];
    }
}
