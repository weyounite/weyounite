<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

class ProjectResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'supporters' => UserResource::collection($this->whenLoaded('supporters')),
            'owner' => new UserResource($this->whenLoaded('owner')),
            'title' => $this->title,
            'description' => $this->description,
            'distance' => $this->when(Str::is('*/search', $request->getPathInfo()), $this->distance),
            'address' => AddressResource::make($this->address),
            'files' => $this->getFileURLs()
        ];
    }

    /**
     * @return Collection
     */
    public function getFileURLs(): Collection
    {
        return $this->getMedia('project-files')->map(function($media) {
            return $media->getURL();
        });
    }
}
