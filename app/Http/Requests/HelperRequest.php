<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class HelperRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //PersonAttributes
            'person.contact' => 'nullable|string',
            'person.has_privacy_settings_accepted' => 'required|bool',
            'person.has_agb_accepted'=> 'required|bool',
            'person.description' => 'nullable|string',
            'person.first_name' => 'nullable|string',
            'person.last_name' => 'nullable|string',
            //AddressAttributes
            'address.lat' => 'nullable|numeric',
            'address.lng' => 'nullable|numeric',
            'address.street' => 'required|string|max:255',
            'address.street_number' => 'required|string|max:50',
            'address.zip' => 'required|string|max:50',
            'address.city' => 'required|string|max:150'
        ];
    }
}
