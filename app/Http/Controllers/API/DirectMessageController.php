<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\SendDirectMessageRequest;
use App\Http\Resources\DirectMessageCollection;
use App\Http\Resources\DirectMessageResource;
use App\Notifications\DirectMessage;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Notifications\DatabaseNotification;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Log;

class DirectMessageController extends Controller
{
    /**
     * Send a message to a specific user
     * @group DirectMessages
     * @urlParam id int required ID of the receiver
     * @bodyParam message string required Example: Die Message die ich heir senden will
     * @bodyParam replyToId string ID of the message I want to reply if it is a reply
     * @response {
        "success": true
        }
     * @param  SendDirectMessageRequest  $request
     * @param  User  $receiver
     * @authenticated
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendMessage(SendDirectMessageRequest $request, User $receiver)
    {
        $success = true;

        try {
            $receiver->notify(
                new DirectMessage(
                    $request->user(),
                    $request->get('message'),
                    $request->get('replyToId', null)
                )
            );
        }
        catch(\Exception $e) {
            Log::critical($e->getMessage(),['exception' => $e]);
            $success = false;
        }

        return response()->json(['success' => $success]);
    }

    /**
     * Mark a message as unread
     * @group DirectMessages
     * @urlParam id string required ID of the message
     * @param  Request  $request
     * @param  DatabaseNotification  $message
     * @authenticated
     * @return \Illuminate\Http\JsonResponse
     */
    public function markAsUnread(Request $request, DatabaseNotification $message)
    {
        $success = true;

        try {
            $message->markAsRead();
        }
        catch(\Exception $e) {
            Log::critical($e->getMessage(),['exception' => $e]);
            $success = false;
        }

        return response()->json(['success' => $success]);
    }

    /**
     * Get messages for an authenticated user
     * @group DirectMessages
     * @param  Request  $request
     * @authenticated
     * @return mixed
     */
    public function get(Request $request)
    {
        return new DirectMessageCollection($request->user()
            ->notifications()
            ->paginate());
    }

    /**
     * Show a specific message
     * @group DirectMessages
     * @urlParam id string required ID of the message
     * @param  Request  $request
     * @param  DatabaseNotification  $message
     * @authenticated
     * @return DirectMessageResource
     */
    public function show(Request $request, DatabaseNotification $message)
    {
        return new DirectMessageResource($message);
    }

    /**
     * Delete a Message
     * @group DirectMessages
     * @urlParam id string required ID of the message
     * @param  Request  $request
     * @param  DatabaseNotification  $message
     * @return \Illuminate\Http\JsonResponse
     * @authenticated
     * @throws \Exception
     */
    public function delete(Request $request,  DatabaseNotification $message)
    {
        $success = $message->delete();

        return response()->json(['success' => (bool) $success]);

    }
}
