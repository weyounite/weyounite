<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProjectsSupportedRequest;
use App\Http\Resources\ProjectsSupportedCollection;
use App\Http\Resources\ProjectsSupportedResource;
use App\ProjectsSupported;
use Illuminate\Http\Request;

class ProjectsSupportedResourceController extends Controller
{
    /**
     * Get the projects supported
     * @group SupportedProjects
     * @authenticated
     * @return ProjectsSupportedResource
     */
    public function index()
    {
        return new ProjectsSupportedCollection(ProjectsSupported::paginate());
    }

    /**
     * Support a project
     * @group SupportedProjects
     * @bodyParam user_id int required ID of the supporter
     * @bodyParam project_id int required ID of the project
     * @authenticated
     * @param  ProjectsSupportedRequest  $request
     * @return ProjectsSupportedResource
     */
    public function store(ProjectsSupportedRequest $request)
    {
        $projectsSupported = ProjectsSupported::create($request->validated());

        return new ProjectsSupportedResource($projectsSupported);
    }

    /**
     * Show a specific support
     * @group SupportedProjects
     * @urlParam id required ID of the resource you want to fetch
     * @param  ProjectsSupported  $projectsSupported
     * @authenticated
     * @return ProjectsSupportedResource
     */
    public function show(ProjectsSupported $projectsSupported)
    {
        return new ProjectsSupportedResource($projectsSupported);
    }

    /**
     * Update a support
     * @group SupportedProjects
     * @urlParam id required ID of the resource you want to update
     * @bodyParam user_id int required ID of the supporter
     * @bodyParam project_id int required ID of the project
     * @param  ProjectsSupportedRequest  $request
     * @param  ProjectsSupported  $projectsSupported
     * @authenticated
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(ProjectsSupportedRequest $request, ProjectsSupported $projectsSupported)
    {
        $projectsSupported->update($request->validated());

        return new ProjectsSupportedResource($projectsSupported->fresh());
    }

    /**
     * Delete a support
     * @group SupportedProjects
     * @urlParam id int required ID of the resource you want to delete
     * @param  ProjectsSupported  $projectsSupported
     * @return \Illuminate\Http\JsonResponse
     * @authenticated
     * @throws \Exception
     */
    public function destroy(ProjectsSupported $projectsSupported)
    {
        $success = $projectsSupported->delete();

        return response()->json([
            'success' => $success
        ]);
    }
}
