<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\FileUploadRequest;
use App\Http\Requests\UserRequest;
use App\Http\Resources\UserCollection;
use App\Http\Resources\UserResource;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class UserResourceController extends Controller
{
    /**
     * Get User
     * @group UserManagement
     * @return UserResource
     */
    public function index()
    {
        return new UserCollection(User::with(['projectsOwned', 'projectsSupported'])->paginate());
    }

    /**
     * Get a specific user
     * @group UserManagement
     * @urlParam id int required ID of the resource you want to fetch
     * @param  User  $user
     * @return UserResource
     */
    public function show(User $user)
    {
        return new UserResource($user);
    }

    /**
     *
     * @group UserManagement
     * @urlParam id required ID of the resource you want to update
     * @param  UserRequest  $request
     * @param  User  $user
     * @return UserResource
     */
    public function update(UserRequest $request, User $user)
    {
        $user->update($request->validated());

        return new UserResource($user->fresh());
    }

    /**
     * Delete an user
     * @group UserManagement
     * @urlParam id required ID of the resource you want to delete
     * @param  User  $user
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(User $user)
    {
        $success = $user->delete();

        return response()->json([
            'success' => $success
        ]);
    }

    /**
     * Upload file for the user
     * @group UserManagement
     * @urlParam id required ID of the resource where you want to upload a file
     * @bodyParam file required A file you want to upload
     * @param  FileUploadRequest  $request
     * @param  User  $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function upload(FileUploadRequest $request, User $user)
    {
        $success = true;

        try {
            $user->addMediaFromRequest('file')
                ->toMediaCollection('user-files');
        }
        catch (\Exception $e) {
            Log::critical($e->getMessage(), ['exception' => $e]);
            $success = false;
        }



        return response()->json(['success' => $success]);
    }
}
