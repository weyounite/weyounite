<?php

namespace App\Http\Controllers\API;

use App\Address;
use App\Http\Controllers\Controller;
use App\Http\Requests\FileUploadRequest;
use App\Http\Requests\ProjectRequest;
use App\Http\Requests\ProjectSearchRequest;
use App\Http\Resources\ProjectCollection;
use App\Http\Resources\ProjectResource;
use App\Project;
use App\ProjectsOwned;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;

class ProjectResourceController extends Controller
{
    /**
     * Search Projects
     * @group Projects
     * @bodyParam lat decimal required
     * @bodyParam lng decimal required
     * @bodyParam radius integer required
     * @param  ProjectSearchRequest  $request
     * @return ProjectResource
     */
    public function search(ProjectSearchRequest $request)
    {
        return new ProjectCollection(Project::radius(
            $request->get('lat'),
            $request->get('lng'),
            $request->get('radius')
        )->with([
            'owner', 'supporters'
        ])->get());

    }

    /**
     * Get projects
     * @group Projects
     * @return ProjectResource
     * @authenticated
     */
    public function index()
    {
        return new ProjectCollection(Project::with([
            'owner', 'supporters'
        ])->paginate());
    }

    /**
     * Create a project
     * @group Projects
     * @bodyParam title string required
     * @bodyParam description string required
     * @bodyParam address.lat float Example: 50.09898090
     * @bodyParam address.lng float Example: 52.98786799
     * @bodyParam address.street string required max 255 Example: Hauptstrasse
     * @bodyParam address.street_number string required max 50 Example: 45a
     * @bodyParam address.zip string required max 50 Example: 08678
     * @bodyParam address.city string required max 150 Example: Buxtehude
     * @param  ProjectRequest  $request
     * @return \Illuminate\Http\JsonResponse
     * @authenticated
     */
    public function store(ProjectRequest $request)
    {
        $project = Project::create(collect($request->validated())->only(
            (new Project())->getFillable())->toArray());

        $addressData = Arr::first(collect($request->validated())->only(
            'address'
        )->values()->toArray());

        $project->address()->create($addressData);

        return new ProjectResource($project);
    }

    /**
     * Get a specific project
     * @group Projects
     * @urlParam id required ID of the resource you want to show
     * @authenticated
     * @param  Project  $project
     * @return ProjectResource
     */
    public function show(Project $project)
    {
        return new ProjectResource($project->load([
            'owner', 'supporters'
        ]));
    }

    /**
     * Update a project
     * @group Projects
     * @urlParam id required ID of the resource you want to update
     * @bodyParam title string required
     * @bodyParam description string required
     * @bodyParam address.lat float Example: 50.09898090
     * @bodyParam address.lng float Example: 52.98786799
     * @bodyParam address.street string required max 255 Example: Hauptstrasse
     * @bodyParam address.street_number string required max 50 Example: 45a
     * @bodyParam address.zip string required max 50 Example: 08678
     * @bodyParam address.city string required max 150 Example: Buxtehude
     * @authenticated
     * @param  ProjectRequest  $request
     * @param  Project  $project
     * @return ProjectResource
     */
    public function update(ProjectRequest $request, Project $project)
    {
        $project->update(collect($request->validated())->only(
            (new Project())->getFillable())->toArray());

        $addressData = Arr::first(collect($request->validated())->only(
            'address'
        )->values()->toArray());

        if ($project->address instanceof Address) {
            $project->address->update($addressData);

        } else{
            $project->address()->create($addressData);
        }

        return new ProjectResource($project->fresh());
    }

    /**
     * Delete a project
     * @group Projects
     * @urlParam id required ID of the resource you want to delete
     * @authenticated
     * @param  Project  $project
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(Project $project)
    {
        $success = $project->delete();

        return response()->json([
            'sucess' => (bool) $success
        ]);
    }

    /**
     * Upload a file to a project
     * @group Projects
     * @urlParam id int required ID of the resource where you want to upload a file
     * @bodyParam file required A file you want to upload
     * @authenticated
     * @param  FileUploadRequest  $request
     * @param  Project  $project
     * @return \Illuminate\Http\JsonResponse
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\DiskDoesNotExist
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileDoesNotExist
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileIsTooBig
     */
    public function upload(FileUploadRequest $request, Project $project)
    {
        $success = true;

        try {
            $project->addMediaFromRequest('file')
                ->toMediaCollection('project-files');
        }
        catch (\Exception $e) {
            Log::critical($e->getMessage(), ['exception' => $e]);
            $success = false;
        }



        return response()->json(['success' => $success]);
    }
}
