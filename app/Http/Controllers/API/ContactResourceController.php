<?php

namespace App\Http\Controllers\API;

use App\Contact;
use App\Http\Controllers\Controller;
use App\Http\Requests\ContactFormRequest;
use App\Http\Resources\ContactResource;
use Illuminate\Http\Request;

class ContactResourceController extends Controller
{
    /**
     * Send a contact message to the weyounite slack channel
     * @group ContactForm
     * @bodyParam email string required
     * @bodyParam concern string required send your message
     * @param  ContactFormRequest  $request
     * @return ContactResource
     */
    public function store(ContactFormRequest $request)
    {
        $contact = Contact::create($request->validated());

        return new ContactResource($contact);
    }
}
