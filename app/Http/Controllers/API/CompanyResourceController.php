<?php

namespace App\Http\Controllers\API;

use App\Address;
use App\Company;
use App\Http\Controllers\Controller;
use App\Http\Requests\CompanyRequest;
use App\Http\Resources\UserCollection;
use App\Http\Resources\UserResource;
use App\Person;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;

class CompanyResourceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Create a company
     * @group Companies
     * @authenticated
     * @urlParam id int required ID of the user
     * @bodyParam name string required Example: Werkstatt A
     * @bodyParam established_at date Example: 2019-01-15
     * @bodyParam stuff_count int Example: 5
     * @bodyParam description string Example: Unternehmensbeschreibung hier
     * @bodyParam challenges string Example: Meine Herausforderungen sind ....
     * @bodyParam strengths string Example: Ich bin gut in ...
     * @bodyParam person.contact string Example: mymail.mail.net
     * @bodyParam person.has_privacy_settings_accepted boolean required Example:false
     * @bodyParam person.has_agb_accepted boolean required Example: true
     * @bodyParam person.description string Example: das ist meine Beschreibung
     * @bodyParam person.first_name string Example: Daniel
     * @bodyParam person.last_name string Example: Koch
     * @bodyParam address.lat float Example: 50.09898090
     * @bodyParam address.lng float Example: 52.98786799
     * @bodyParam address.street string required max 255 Example: Hauptstrasse
     * @bodyParam address.street_number string required max 50 Example: 45a
     * @bodyParam address.zip string required max 50 Example: 08678
     * @bodyParam address.city string required max 150 Example: Buxtehude
     * @response {
        "data": {
        "id": 1,
        "name": "Dominic Dickens",
        "email": "rosalind.spencer@example.com",
        "person": {
        "id": 1,
        "user_id": 1,
        "contact": null,
        "description": "voluptate culpa illum optio qui",
        "first_name": "Heath",
        "last_name": "Luettgen",
        "has_privacy_settings_accepted": false,
        "has_agb_accepted": false,
        "privacy_settings_accepted_at": null,
        "agb_accepted_at": null,
        "created_at": "2020-04-04T18:14:48.000000Z",
        "updated_at": "2020-04-04T18:14:48.000000Z",
        "deleted_at": null
        },
        "helper": {
        "address": {
        "id": 1,
        "addressable_id": 1,
        "addressable_type": "App\\Helper",
        "street": "Alba Islands",
        "street_number": "Glens",
        "zip": "42609",
        "city": "Kossborough",
        "lat": "50.27502400",
        "lng": "-85.88578700",
        "created_at": "2020-04-04T18:14:48.000000Z",
        "updated_at": "2020-04-04T18:14:48.000000Z",
        "deleted_at": null
        }
        },
        "company": {
        "established_at": "2020-01-01T00:00:00.000000Z",
        "name": "qui",
        "staff_count": 1,
        "description": "molestiae",
        "challenges": "culpa",
        "strengths": "consectetur",
        "address": {
        "id": 22,
        "addressable_id": 11,
        "addressable_type": "App\\Company",
        "street": "a",
        "street_number": "amet",
        "zip": "sint",
        "city": "cum",
        "lat": null,
        "lng": null,
        "created_at": "2020-04-04T18:40:17.000000Z",
        "updated_at": "2020-04-04T18:40:17.000000Z",
        "deleted_at": null
        }
        },
        "files": []
        }
        }
     * @param  CompanyRequest  $request
     * @param  User  $user
     * @return UserResource|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function store(CompanyRequest $request, User $user)
    {
        try {
            $user->company()->create(collect($request->validated())->only(
                (new Company())->getFillable()
            )->toArray());

            $personData = Arr::first(collect($request->validated())->only(
                'person'
            )->values()->toArray());

            $user->person()->create($personData);

            $addressData = Arr::first(collect($request->validated())->only(
                'address'
            )->values()->toArray());

            $user->company->address()->create($addressData);
        }catch (\Exception $e) {
            Log::critical($e->getMessage(),[$e]);
            return response('something went wrong', 422);
        }

        return new UserResource($user->fresh());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function show(Company $company)
    {
        //
    }

    /**
     * Update a company
     * @group Companies
     * @authenticated
     * @urlParam id int required ID of the company
     * @bodyParam name string required Example: Werkstatt A
     * @bodyParam established_at date Example: 2019-01-15
     * @bodyParam stuff_count int Example: 5
     * @bodyParam description string Example: Unternehmensbeschreibung hier
     * @bodyParam challenges string Example: Meine Herausforderungen sind ....
     * @bodyParam strengths string Example: Ich bin gut in ...
     * @bodyParam person.contact string Example: mymail.mail.net
     * @bodyParam person.has_privacy_settings_accepted boolean required Example:false
     * @bodyParam person.has_agb_accepted boolean required Example: true
     * @bodyParam person.description string Example: das ist meine Beschreibung
     * @bodyParam person.first_name string Example: Daniel
     * @bodyParam person.last_name string Example: Koch
     * @bodyParam address.lat float Example: 50.09898090
     * @bodyParam address.lng float Example: 52.98786799
     * @bodyParam address.street string required max 255 Example: Hauptstrasse
     * @bodyParam address.street_number string required max 50 Example: 45a
     * @bodyParam address.zip string required max 50 Example: 08678
     * @bodyParam address.city string required max 150 Example: Buxtehude
     * @response {
        "data": {
        "id": 1,
        "name": "Dominic Dickens",
        "email": "rosalind.spencer@example.com",
        "person": {
        "id": 1,
        "user_id": 1,
        "contact": null,
        "description": "voluptate culpa illum optio qui",
        "first_name": "Heath",
        "last_name": "Luettgen",
        "has_privacy_settings_accepted": false,
        "has_agb_accepted": false,
        "privacy_settings_accepted_at": null,
        "agb_accepted_at": null,
        "created_at": "2020-04-04T18:14:48.000000Z",
        "updated_at": "2020-04-04T18:14:48.000000Z",
        "deleted_at": null
        },
        "helper": {
        "address": {
        "id": 1,
        "addressable_id": 1,
        "addressable_type": "App\\Helper",
        "street": "Alba Islands",
        "street_number": "Glens",
        "zip": "42609",
        "city": "Kossborough",
        "lat": "50.27502400",
        "lng": "-85.88578700",
        "created_at": "2020-04-04T18:14:48.000000Z",
        "updated_at": "2020-04-04T18:14:48.000000Z",
        "deleted_at": null
        }
        },
        "company": {
        "established_at": "2020-01-01T00:00:00.000000Z",
        "name": "qui",
        "staff_count": 1,
        "description": "molestiae",
        "challenges": "culpa",
        "strengths": "consectetur",
        "address": {
        "id": 22,
        "addressable_id": 11,
        "addressable_type": "App\\Company",
        "street": "a",
        "street_number": "amet",
        "zip": "sint",
        "city": "cum",
        "lat": null,
        "lng": null,
        "created_at": "2020-04-04T18:40:17.000000Z",
        "updated_at": "2020-04-04T18:40:17.000000Z",
        "deleted_at": null
        }
        },
        "files": []
        }
        }
     * @param  CompanyRequest  $request
     * @param  User  $user
     * @return UserResource|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function update(CompanyRequest $request, Company $company)
    {
        try {
            $company->update(collect($request->validated())->only(
                (new Company())->getFillable()
            )->toArray());

            $personData = Arr::first(collect($request->validated())->only(
                'person'
            )->values()->toArray());

            $company->user->person()->update($personData);

            $addressData = Arr::first(collect($request->validated())->only(
                'address'
            )->values()->toArray());

            $company->address()->update($addressData);
        }catch (\Exception $e) {
            Log::critical($e->getMessage(),[$e]);
            return response('something went wrong', 422);
        }

        return new UserResource($company->user->fresh());
    }

    /**
     * Delete a company
     * @group Companies
     * @authenticated
     * @param  Company  $company
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(Company $company)
    {
        $success = $company->delete();

        return response()->json([
            'success' => $success
        ]);
    }
}
