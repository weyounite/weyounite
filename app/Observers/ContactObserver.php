<?php

namespace App\Observers;

use App\Contact;
use App\Notifications\ContactCreated;
use Illuminate\Support\Facades\Notification;

class ContactObserver
{
    /**
     * Handle the contact "created" event.
     *
     * @param  \App\Contact  $contact
     * @return void
     */
    public function created(Contact $contact)
    {
        Notification::route('slack', env('SLACK_CHANNEL_CONTACTCREATED'))
            ->notify(new ContactCreated($contact));
    }
}
