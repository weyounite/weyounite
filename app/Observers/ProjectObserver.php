<?php

namespace App\Observers;

use App\Project;
use App\ProjectsOwned;

class ProjectObserver
{
    /**
     * Handle the project "created" event.
     *
     * @param  \App\Project  $project
     * @return void
     */
    public function created(Project $project)
    {
        if (!auth()->guest()) {
            ProjectsOwned::create([
                'user_id' => auth()->user()->id,
                'project_id' => $project->id
            ]);
        }
    }
}
