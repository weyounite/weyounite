<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class Address extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'street', 'street_number', 'zip', 'city', 'lat', 'lng', 'addressable_type', 'addressable_id'
    ];

    protected $casts = [
        'lat' => 'decimal:8',
        'lng' => 'decimal:8'
    ];

    /**
     * @return MorphTo
     */
    public function addressable(): MorphTo
    {
        return $this->morphTo();
    }

    public function scopeRadius($query, $location_latitude, $location_longitude, $radius = 10) {
        $haversine = "(
            6371 * acos
            (
                cos(
                    radians($location_latitude)
                )
                * cos(
                    radians( lat )
                ) * cos(
                    radians( lng ) - radians($location_longitude)
                ) + sin(
                    radians($location_latitude)
                ) * sin(
                    radians( lat )
                )
            )
        )";

        return $query
            ->select('*')
            ->selectRaw("{$haversine} AS distance")
            ->having("distance", "<", $radius)
            ->orderBy('distance', 'asc');
    }
}
