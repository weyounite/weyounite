<?php

namespace App\Notifications;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class DirectMessage extends Notification
{
    use Queueable;

    /** @var string  */
    public $message;

    /** @var User  */
    public $sender;

    /**
     * @var string|null
     */
    public $replyToId;

    /**
     * DirectMessage constructor.
     * @param  User  $sender
     * @param  string  $message
     * @param  string|null  $replyToId
     */
    public function __construct(User $sender, string $message, string $replyToId = null)
    {
        $this->sender = $sender;
        $this->message = $message;
        $this->replyToId = $replyToId;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->greeting('Hallo!')
            ->line('Es gibt eine neue Nachricht für dich von ' . $this->sender->name)
            ->line($this->message);
    }

    public function toDatabase($notifiable)
    {
        return [
            'senderId' => $this->sender->id,
            'message' => $this->message,
            'replyToId' => $this->replyToId
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'sender' => $this->sender,
            'message' => $this->message,
            'replyToId' => $this->replyToId
        ];
    }
}
