<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class ProjectsSupported extends Model
{
    protected $table = "projectsSupported";

    protected $guarded = [];

    public function project(): BelongsTo
    {
        return $this->belongsTo(Project::class,'project_id', 'id');
    }

    public function supporter(): BelongsTo
    {
        return $this->belongsTo(User::class,'user_id', 'id');
    }
}
