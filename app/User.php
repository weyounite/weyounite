<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class User extends Authenticatable implements HasMedia
{
    use Notifiable, SoftDeletes, HasMediaTrait, HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * @var array
     */
    protected $dates = [
        'created_at', 'updated_at', 'deleted_at', 'privacy_settings_accepted_at', 'agb_accepted_at'
    ];

    /**
     * using https://docs.spatie.be/laravel-medialibrary/v7
     */
    public function registerMediaCollections()
    {
        $this->addMediaCollection('user-files')
            ->useDisk('public');
    }

    public function person(): HasOne
    {
        return $this->hasOne(Person::class);
    }

    /**
     * @return HasOne
     */
    public function helper(): HasOne
    {
        return $this->hasOne(Helper::class);
    }

    /**
     * @return HasOne
     */
    public function company(): HasOne
    {
        return $this->hasOne(Company::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function projectsOwned()
    {
        return $this->hasManyThrough(
            Project::class,
            ProjectsOwned::class,
            'user_id',
            'id',
            'id',
            'project_id'
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function projectsSupported()
    {
        return $this->hasManyThrough(
            Project::class,
            ProjectsSupported::class,
            'user_id',
            'id',
            'project_id'
        );
    }
}
