<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class Person extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'contact', 'has_privacy_settings_accepted', 'has_agb_accepted', 'description',
        'privacy_settings_accepted_at', 'agb_accepted_at', 'first_name', 'last_name'
    ];

    /**
     * @var array
     */
    protected $dates = [
        'created_at', 'updated_at', 'deleted_at', 'privacy_settings_accepted_at', 'agb_accepted_at'
    ];

    protected $casts = [
        'has_privacy_settings_accepted' => 'boolean',
        'has_agb_accepted' => 'boolean',
        'lat' => 'decimal:8',
        'lng' => 'decimal:8'
    ];

    public function user():BelongsTo
    {
        return $this->belongsTo(User::class);
    }

}
