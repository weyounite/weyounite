<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class Project extends Model implements HasMedia
{
    use SoftDeletes, HasMediaTrait;

    protected $fillable = ['title', 'description', 'lat', 'lng', 'contact'];

    protected $casts = [
        'lat' => 'decimal:8',
        'lng' => 'decimal:8'
    ];

    /**
     * using https://docs.spatie.be/laravel-medialibrary/v7
     */
    public function registerMediaCollections()
    {
        $this->addMediaCollection('project-files')
            ->useDisk('public');
    }

    public function supporters()
    {
        return $this->hasManyThrough(
            User::class,
            ProjectsSupported::class,
            'project_id',
            'id',
            'id',
            'user_id'
        );
    }

    public function owner()
    {
        return $this->hasOneThrough(
            User::class,
            ProjectsOwned::class,
            'project_id',
            'id',
            'id',
            'user_id'
        );
    }

    /**
     * @return MorphOne
     */
    public function address(): MorphOne
    {
        return $this->morphOne(Address::class, 'addressable');
    }

    public function scopeRadius($query, $location_latitude, $location_longitude, $radius = 10) {
        $condition = "(
            6371 * acos
            (
                cos(
                    radians($location_latitude)
                )
                * cos(
                    radians( lat )
                ) * cos(
                    radians( lng ) - radians($location_longitude)
                ) + sin(
                    radians($location_latitude)
                ) * sin(
                    radians( lat )
                )
            )
        )";

        return $query
            ->whereHas('address', function ($query) use($condition, $radius) {
                $query->select('*')
                    ->selectRaw("{$condition} AS distance")
                    ->having("distance", "<", $radius)
                    ->orderBy('distance', 'asc');
            });
    }
}
