---
title: API Reference

language_tabs:
- bash
- javascript
- php

includes:

search: true

toc_footers:
- <a href='http://github.com/mpociot/documentarian'>Documentation Powered by Documentarian</a>
---
<!-- START_INFO -->
# Info

Welcome to the generated API reference.
[Get Postman Collection](http://weyounite.dev/docs/collection.json)

<!-- END_INFO -->

#Auth


<!-- START_7b9ab09012dc67331af4f642debe17a0 -->
## Get a bearer token to authenticate

> Example request:

```bash
curl -X POST \
    "https://weyounite.dev/api/auth/token" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}" \
    -d '{"email":"ipsum","password":"ipsam","device_name":"perspiciatis"}'

```

```javascript
const url = new URL(
    "https://weyounite.dev/api/auth/token"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

let body = {
    "email": "ipsum",
    "password": "ipsam",
    "device_name": "perspiciatis"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://weyounite.dev/api/auth/token',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
            'Authorization' => 'Bearer {token}',
        ],
        'json' => [
            'email' => 'ipsum',
            'password' => 'ipsam',
            'device_name' => 'perspiciatis',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`POST api/auth/token`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `email` | string |  required  | 
        `password` | string |  required  | 
        `device_name` | string |  required  | 
    
<!-- END_7b9ab09012dc67331af4f642debe17a0 -->

#Companies


<!-- START_1e6a34851b0689db52677b43727419b5 -->
## Update a company

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X PUT \
    "https://weyounite.dev/api/companies/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}" \
    -d '{"name":"Werkstatt A","established_at":"2019-01-15","stuff_count":5,"description":"Unternehmensbeschreibung hier","challenges":"Meine Herausforderungen sind ....","strengths":"Ich bin gut in ...","person":{"contact":"mymail.mail.net","has_privacy_settings_accepted":false,"has_agb_accepted":true,"description":"das ist meine Beschreibung","first_name":"Daniel","last_name":"Koch"},"address":{"lat":50.0989809,"lng":52.98786799,"street":"Hauptstrasse","street_number":"45a","zip":"08678","city":"Buxtehude"}}'

```

```javascript
const url = new URL(
    "https://weyounite.dev/api/companies/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

let body = {
    "name": "Werkstatt A",
    "established_at": "2019-01-15",
    "stuff_count": 5,
    "description": "Unternehmensbeschreibung hier",
    "challenges": "Meine Herausforderungen sind ....",
    "strengths": "Ich bin gut in ...",
    "person": {
        "contact": "mymail.mail.net",
        "has_privacy_settings_accepted": false,
        "has_agb_accepted": true,
        "description": "das ist meine Beschreibung",
        "first_name": "Daniel",
        "last_name": "Koch"
    },
    "address": {
        "lat": 50.0989809,
        "lng": 52.98786799,
        "street": "Hauptstrasse",
        "street_number": "45a",
        "zip": "08678",
        "city": "Buxtehude"
    }
}

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->put(
    'https://weyounite.dev/api/companies/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
            'Authorization' => 'Bearer {token}',
        ],
        'json' => [
            'name' => 'Werkstatt A',
            'established_at' => '2019-01-15',
            'stuff_count' => 5,
            'description' => 'Unternehmensbeschreibung hier',
            'challenges' => 'Meine Herausforderungen sind ....',
            'strengths' => 'Ich bin gut in ...',
            'person' => [
                'contact' => 'mymail.mail.net',
                'has_privacy_settings_accepted' => false,
                'has_agb_accepted' => true,
                'description' => 'das ist meine Beschreibung',
                'first_name' => 'Daniel',
                'last_name' => 'Koch',
            ],
            'address' => [
                'lat' => 50.0989809,
                'lng' => 52.98786799,
                'street' => 'Hauptstrasse',
                'street_number' => '45a',
                'zip' => '08678',
                'city' => 'Buxtehude',
            ],
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "data": {
        "id": 1,
        "name": "Dominic Dickens",
        "email": "rosalind.spencer@example.com",
        "person": {
            "id": 1,
            "user_id": 1,
            "contact": null,
            "description": "voluptate culpa illum optio qui",
            "first_name": "Heath",
            "last_name": "Luettgen",
            "has_privacy_settings_accepted": false,
            "has_agb_accepted": false,
            "privacy_settings_accepted_at": null,
            "agb_accepted_at": null,
            "created_at": "2020-04-04T18:14:48.000000Z",
            "updated_at": "2020-04-04T18:14:48.000000Z",
            "deleted_at": null
        },
        "helper": {
            "address": {
                "id": 1,
                "addressable_id": 1,
                "addressable_type": "App\\Helper",
                "street": "Alba Islands",
                "street_number": "Glens",
                "zip": "42609",
                "city": "Kossborough",
                "lat": "50.27502400",
                "lng": "-85.88578700",
                "created_at": "2020-04-04T18:14:48.000000Z",
                "updated_at": "2020-04-04T18:14:48.000000Z",
                "deleted_at": null
            }
        },
        "company": {
            "established_at": "2020-01-01T00:00:00.000000Z",
            "name": "qui",
            "staff_count": 1,
            "description": "molestiae",
            "challenges": "culpa",
            "strengths": "consectetur",
            "address": {
                "id": 22,
                "addressable_id": 11,
                "addressable_type": "App\\Company",
                "street": "a",
                "street_number": "amet",
                "zip": "sint",
                "city": "cum",
                "lat": null,
                "lng": null,
                "created_at": "2020-04-04T18:40:17.000000Z",
                "updated_at": "2020-04-04T18:40:17.000000Z",
                "deleted_at": null
            }
        },
        "files": []
    }
}
```

### HTTP Request
`PUT api/companies/{company}`

`PATCH api/companies/{company}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `id` |  optional  | int required ID of the company
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `name` | string |  required  | 
        `established_at` | date |  optional  | 
        `stuff_count` | integer |  optional  | 
        `description` | string |  optional  | 
        `challenges` | string |  optional  | 
        `strengths` | string |  optional  | 
        `person.contact` | string |  optional  | 
        `person.has_privacy_settings_accepted` | boolean |  required  | 
        `person.has_agb_accepted` | boolean |  required  | 
        `person.description` | string |  optional  | 
        `person.first_name` | string |  optional  | 
        `person.last_name` | string |  optional  | 
        `address.lat` | float |  optional  | 
        `address.lng` | float |  optional  | 
        `address.street` | string |  required  | max 255
        `address.street_number` | string |  required  | max 50
        `address.zip` | string |  required  | max 50
        `address.city` | string |  required  | max 150
    
<!-- END_1e6a34851b0689db52677b43727419b5 -->

<!-- START_72de66eabebc78e1d0e514081409da3a -->
## Delete a company

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X DELETE \
    "https://weyounite.dev/api/companies/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "https://weyounite.dev/api/companies/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->delete(
    'https://weyounite.dev/api/companies/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
            'Authorization' => 'Bearer {token}',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`DELETE api/companies/{company}`


<!-- END_72de66eabebc78e1d0e514081409da3a -->

<!-- START_4bc61bb957ffc4533cf83fc0d656fc9f -->
## Create a company

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "https://weyounite.dev/api/companies/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}" \
    -d '{"name":"Werkstatt A","established_at":"2019-01-15","stuff_count":5,"description":"Unternehmensbeschreibung hier","challenges":"Meine Herausforderungen sind ....","strengths":"Ich bin gut in ...","person":{"contact":"mymail.mail.net","has_privacy_settings_accepted":false,"has_agb_accepted":true,"description":"das ist meine Beschreibung","first_name":"Daniel","last_name":"Koch"},"address":{"lat":50.0989809,"lng":52.98786799,"street":"Hauptstrasse","street_number":"45a","zip":"08678","city":"Buxtehude"}}'

```

```javascript
const url = new URL(
    "https://weyounite.dev/api/companies/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

let body = {
    "name": "Werkstatt A",
    "established_at": "2019-01-15",
    "stuff_count": 5,
    "description": "Unternehmensbeschreibung hier",
    "challenges": "Meine Herausforderungen sind ....",
    "strengths": "Ich bin gut in ...",
    "person": {
        "contact": "mymail.mail.net",
        "has_privacy_settings_accepted": false,
        "has_agb_accepted": true,
        "description": "das ist meine Beschreibung",
        "first_name": "Daniel",
        "last_name": "Koch"
    },
    "address": {
        "lat": 50.0989809,
        "lng": 52.98786799,
        "street": "Hauptstrasse",
        "street_number": "45a",
        "zip": "08678",
        "city": "Buxtehude"
    }
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://weyounite.dev/api/companies/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
            'Authorization' => 'Bearer {token}',
        ],
        'json' => [
            'name' => 'Werkstatt A',
            'established_at' => '2019-01-15',
            'stuff_count' => 5,
            'description' => 'Unternehmensbeschreibung hier',
            'challenges' => 'Meine Herausforderungen sind ....',
            'strengths' => 'Ich bin gut in ...',
            'person' => [
                'contact' => 'mymail.mail.net',
                'has_privacy_settings_accepted' => false,
                'has_agb_accepted' => true,
                'description' => 'das ist meine Beschreibung',
                'first_name' => 'Daniel',
                'last_name' => 'Koch',
            ],
            'address' => [
                'lat' => 50.0989809,
                'lng' => 52.98786799,
                'street' => 'Hauptstrasse',
                'street_number' => '45a',
                'zip' => '08678',
                'city' => 'Buxtehude',
            ],
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "data": {
        "id": 1,
        "name": "Dominic Dickens",
        "email": "rosalind.spencer@example.com",
        "person": {
            "id": 1,
            "user_id": 1,
            "contact": null,
            "description": "voluptate culpa illum optio qui",
            "first_name": "Heath",
            "last_name": "Luettgen",
            "has_privacy_settings_accepted": false,
            "has_agb_accepted": false,
            "privacy_settings_accepted_at": null,
            "agb_accepted_at": null,
            "created_at": "2020-04-04T18:14:48.000000Z",
            "updated_at": "2020-04-04T18:14:48.000000Z",
            "deleted_at": null
        },
        "helper": {
            "address": {
                "id": 1,
                "addressable_id": 1,
                "addressable_type": "App\\Helper",
                "street": "Alba Islands",
                "street_number": "Glens",
                "zip": "42609",
                "city": "Kossborough",
                "lat": "50.27502400",
                "lng": "-85.88578700",
                "created_at": "2020-04-04T18:14:48.000000Z",
                "updated_at": "2020-04-04T18:14:48.000000Z",
                "deleted_at": null
            }
        },
        "company": {
            "established_at": "2020-01-01T00:00:00.000000Z",
            "name": "qui",
            "staff_count": 1,
            "description": "molestiae",
            "challenges": "culpa",
            "strengths": "consectetur",
            "address": {
                "id": 22,
                "addressable_id": 11,
                "addressable_type": "App\\Company",
                "street": "a",
                "street_number": "amet",
                "zip": "sint",
                "city": "cum",
                "lat": null,
                "lng": null,
                "created_at": "2020-04-04T18:40:17.000000Z",
                "updated_at": "2020-04-04T18:40:17.000000Z",
                "deleted_at": null
            }
        },
        "files": []
    }
}
```

### HTTP Request
`POST api/companies/{user}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `id` |  optional  | int required ID of the user
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `name` | string |  required  | 
        `established_at` | date |  optional  | 
        `stuff_count` | integer |  optional  | 
        `description` | string |  optional  | 
        `challenges` | string |  optional  | 
        `strengths` | string |  optional  | 
        `person.contact` | string |  optional  | 
        `person.has_privacy_settings_accepted` | boolean |  required  | 
        `person.has_agb_accepted` | boolean |  required  | 
        `person.description` | string |  optional  | 
        `person.first_name` | string |  optional  | 
        `person.last_name` | string |  optional  | 
        `address.lat` | float |  optional  | 
        `address.lng` | float |  optional  | 
        `address.street` | string |  required  | max 255
        `address.street_number` | string |  required  | max 50
        `address.zip` | string |  required  | max 50
        `address.city` | string |  required  | max 150
    
<!-- END_4bc61bb957ffc4533cf83fc0d656fc9f -->

#ContactForm


<!-- START_d82defe8489483fff70f435f9c68ce2d -->
## Send a contact message to the weyounite slack channel

> Example request:

```bash
curl -X POST \
    "https://weyounite.dev/api/contact/request" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}" \
    -d '{"email":"reprehenderit","concern":"quibusdam"}'

```

```javascript
const url = new URL(
    "https://weyounite.dev/api/contact/request"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

let body = {
    "email": "reprehenderit",
    "concern": "quibusdam"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://weyounite.dev/api/contact/request',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
            'Authorization' => 'Bearer {token}',
        ],
        'json' => [
            'email' => 'reprehenderit',
            'concern' => 'quibusdam',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`POST api/contact/request`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `email` | string |  required  | 
        `concern` | string |  required  | send your message
    
<!-- END_d82defe8489483fff70f435f9c68ce2d -->

#DirectMessages


<!-- START_c61e9c2b3fdeea56ee207c8db3d88546 -->
## Get messages for an authenticated user

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "https://weyounite.dev/api/messages" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "https://weyounite.dev/api/messages"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://weyounite.dev/api/messages',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
            'Authorization' => 'Bearer {token}',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/messages`


<!-- END_c61e9c2b3fdeea56ee207c8db3d88546 -->

<!-- START_cef8438938cd56a8a7a685a273a52336 -->
## Show a specific message

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "https://weyounite.dev/api/messages/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "https://weyounite.dev/api/messages/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://weyounite.dev/api/messages/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
            'Authorization' => 'Bearer {token}',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/messages/{message}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `id` |  optional  | string required ID of the message

<!-- END_cef8438938cd56a8a7a685a273a52336 -->

<!-- START_7b6f93fba506305412683dbf77dd0cc9 -->
## Send a message to a specific user

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "https://weyounite.dev/api/messages/send/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}" \
    -d '{"message":"Die Message die ich heir senden will","replyToId":"deleniti"}'

```

```javascript
const url = new URL(
    "https://weyounite.dev/api/messages/send/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

let body = {
    "message": "Die Message die ich heir senden will",
    "replyToId": "deleniti"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://weyounite.dev/api/messages/send/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
            'Authorization' => 'Bearer {token}',
        ],
        'json' => [
            'message' => 'Die Message die ich heir senden will',
            'replyToId' => 'deleniti',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "success": true
}
```

### HTTP Request
`POST api/messages/send/{receiver}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `id` |  optional  | int required ID of the receiver
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `message` | string |  required  | 
        `replyToId` | string |  optional  | ID of the message I want to reply if it is a reply
    
<!-- END_7b6f93fba506305412683dbf77dd0cc9 -->

<!-- START_6ce1f52a38ffa11186d9904e22bb15aa -->
## Mark a message as unread

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X PUT \
    "https://weyounite.dev/api/messages/mark_as_read/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "https://weyounite.dev/api/messages/mark_as_read/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->put(
    'https://weyounite.dev/api/messages/mark_as_read/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
            'Authorization' => 'Bearer {token}',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`PUT api/messages/mark_as_read/{message}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `id` |  optional  | string required ID of the message

<!-- END_6ce1f52a38ffa11186d9904e22bb15aa -->

#Helper


<!-- START_f8b315cf38f34c37425f3116002b6858 -->
## Create a helper

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X PUT \
    "https://weyounite.dev/api/helper/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}" \
    -d '{"person":{"contact":"mymail.mail.net","has_privacy_settings_accepted":false,"has_agb_accepted":true,"description":"das ist meine Beschreibung","first_name":"Daniel","last_name":"Koch"},"address":{"lat":50.0989809,"lng":52.98786799,"street":"Hauptstrasse","street_number":"45a","zip":"08678","city":"Buxtehude"}}'

```

```javascript
const url = new URL(
    "https://weyounite.dev/api/helper/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

let body = {
    "person": {
        "contact": "mymail.mail.net",
        "has_privacy_settings_accepted": false,
        "has_agb_accepted": true,
        "description": "das ist meine Beschreibung",
        "first_name": "Daniel",
        "last_name": "Koch"
    },
    "address": {
        "lat": 50.0989809,
        "lng": 52.98786799,
        "street": "Hauptstrasse",
        "street_number": "45a",
        "zip": "08678",
        "city": "Buxtehude"
    }
}

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->put(
    'https://weyounite.dev/api/helper/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
            'Authorization' => 'Bearer {token}',
        ],
        'json' => [
            'person' => [
                'contact' => 'mymail.mail.net',
                'has_privacy_settings_accepted' => false,
                'has_agb_accepted' => true,
                'description' => 'das ist meine Beschreibung',
                'first_name' => 'Daniel',
                'last_name' => 'Koch',
            ],
            'address' => [
                'lat' => 50.0989809,
                'lng' => 52.98786799,
                'street' => 'Hauptstrasse',
                'street_number' => '45a',
                'zip' => '08678',
                'city' => 'Buxtehude',
            ],
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "data": {
        "id": 1,
        "name": "Dominic Dickens",
        "email": "rosalind.spencer@example.com",
        "person": {
            "id": 1,
            "user_id": 1,
            "contact": null,
            "description": "voluptate culpa illum optio qui",
            "first_name": "Heath",
            "last_name": "Luettgen",
            "has_privacy_settings_accepted": false,
            "has_agb_accepted": false,
            "privacy_settings_accepted_at": null,
            "agb_accepted_at": null,
            "created_at": "2020-04-04T18:14:48.000000Z",
            "updated_at": "2020-04-04T18:14:48.000000Z",
            "deleted_at": null
        },
        "helper": {
            "address": {
                "id": 1,
                "addressable_id": 1,
                "addressable_type": "App\\Helper",
                "street": "Alba Islands",
                "street_number": "Glens",
                "zip": "42609",
                "city": "Kossborough",
                "lat": "50.27502400",
                "lng": "-85.88578700",
                "created_at": "2020-04-04T18:14:48.000000Z",
                "updated_at": "2020-04-04T18:14:48.000000Z",
                "deleted_at": null
            }
        },
        "company": {
            "established_at": "2020-01-01T00:00:00.000000Z",
            "name": "qui",
            "staff_count": 1,
            "description": "molestiae",
            "challenges": "culpa",
            "strengths": "consectetur",
            "address": {
                "id": 22,
                "addressable_id": 11,
                "addressable_type": "App\\Company",
                "street": "a",
                "street_number": "amet",
                "zip": "sint",
                "city": "cum",
                "lat": null,
                "lng": null,
                "created_at": "2020-04-04T18:40:17.000000Z",
                "updated_at": "2020-04-04T18:40:17.000000Z",
                "deleted_at": null
            }
        },
        "files": []
    }
}
```

### HTTP Request
`PUT api/helper/{helper}`

`PATCH api/helper/{helper}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `id` |  optional  | int required ID of the user
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `person.contact` | string |  optional  | 
        `person.has_privacy_settings_accepted` | boolean |  required  | 
        `person.has_agb_accepted` | boolean |  required  | 
        `person.description` | string |  optional  | 
        `person.first_name` | string |  optional  | 
        `person.last_name` | string |  optional  | 
        `address.lat` | float |  optional  | 
        `address.lng` | float |  optional  | 
        `address.street` | string |  required  | max 255
        `address.street_number` | string |  required  | max 50
        `address.zip` | string |  required  | max 50
        `address.city` | string |  required  | max 150
    
<!-- END_f8b315cf38f34c37425f3116002b6858 -->

<!-- START_1991dcd161689f359a42ce3f5f621125 -->
## Delete a helper

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X DELETE \
    "https://weyounite.dev/api/helper/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "https://weyounite.dev/api/helper/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->delete(
    'https://weyounite.dev/api/helper/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
            'Authorization' => 'Bearer {token}',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`DELETE api/helper/{helper}`


<!-- END_1991dcd161689f359a42ce3f5f621125 -->

<!-- START_4ba978be325a8c0db81a652edffb127a -->
## Create a helper

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "https://weyounite.dev/api/helper/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}" \
    -d '{"person":{"contact":"mymail.mail.net","has_privacy_settings_accepted":false,"has_agb_accepted":true,"description":"das ist meine Beschreibung","first_name":"Daniel","last_name":"Koch"},"address":{"lat":50.0989809,"lng":52.98786799,"street":"Hauptstrasse","street_number":"45a","zip":"08678","city":"Buxtehude"}}'

```

```javascript
const url = new URL(
    "https://weyounite.dev/api/helper/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

let body = {
    "person": {
        "contact": "mymail.mail.net",
        "has_privacy_settings_accepted": false,
        "has_agb_accepted": true,
        "description": "das ist meine Beschreibung",
        "first_name": "Daniel",
        "last_name": "Koch"
    },
    "address": {
        "lat": 50.0989809,
        "lng": 52.98786799,
        "street": "Hauptstrasse",
        "street_number": "45a",
        "zip": "08678",
        "city": "Buxtehude"
    }
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://weyounite.dev/api/helper/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
            'Authorization' => 'Bearer {token}',
        ],
        'json' => [
            'person' => [
                'contact' => 'mymail.mail.net',
                'has_privacy_settings_accepted' => false,
                'has_agb_accepted' => true,
                'description' => 'das ist meine Beschreibung',
                'first_name' => 'Daniel',
                'last_name' => 'Koch',
            ],
            'address' => [
                'lat' => 50.0989809,
                'lng' => 52.98786799,
                'street' => 'Hauptstrasse',
                'street_number' => '45a',
                'zip' => '08678',
                'city' => 'Buxtehude',
            ],
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "data": {
        "id": 1,
        "name": "Dominic Dickens",
        "email": "rosalind.spencer@example.com",
        "person": {
            "id": 1,
            "user_id": 1,
            "contact": null,
            "description": "voluptate culpa illum optio qui",
            "first_name": "Heath",
            "last_name": "Luettgen",
            "has_privacy_settings_accepted": false,
            "has_agb_accepted": false,
            "privacy_settings_accepted_at": null,
            "agb_accepted_at": null,
            "created_at": "2020-04-04T18:14:48.000000Z",
            "updated_at": "2020-04-04T18:14:48.000000Z",
            "deleted_at": null
        },
        "helper": {
            "address": {
                "id": 1,
                "addressable_id": 1,
                "addressable_type": "App\\Helper",
                "street": "Alba Islands",
                "street_number": "Glens",
                "zip": "42609",
                "city": "Kossborough",
                "lat": "50.27502400",
                "lng": "-85.88578700",
                "created_at": "2020-04-04T18:14:48.000000Z",
                "updated_at": "2020-04-04T18:14:48.000000Z",
                "deleted_at": null
            }
        },
        "company": {
            "established_at": "2020-01-01T00:00:00.000000Z",
            "name": "qui",
            "staff_count": 1,
            "description": "molestiae",
            "challenges": "culpa",
            "strengths": "consectetur",
            "address": {
                "id": 22,
                "addressable_id": 11,
                "addressable_type": "App\\Company",
                "street": "a",
                "street_number": "amet",
                "zip": "sint",
                "city": "cum",
                "lat": null,
                "lng": null,
                "created_at": "2020-04-04T18:40:17.000000Z",
                "updated_at": "2020-04-04T18:40:17.000000Z",
                "deleted_at": null
            }
        },
        "files": []
    }
}
```

### HTTP Request
`POST api/helper/{user}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `id` |  optional  | int required ID of the user
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `person.contact` | string |  optional  | 
        `person.has_privacy_settings_accepted` | boolean |  required  | 
        `person.has_agb_accepted` | boolean |  required  | 
        `person.description` | string |  optional  | 
        `person.first_name` | string |  optional  | 
        `person.last_name` | string |  optional  | 
        `address.lat` | float |  optional  | 
        `address.lng` | float |  optional  | 
        `address.street` | string |  required  | max 255
        `address.street_number` | string |  required  | max 50
        `address.zip` | string |  required  | max 50
        `address.city` | string |  required  | max 150
    
<!-- END_4ba978be325a8c0db81a652edffb127a -->

#Projects


<!-- START_dd06cfd8ba2a7245555c311ab39a6f11 -->
## Search Projects

> Example request:

```bash
curl -X POST \
    "https://weyounite.dev/api/projects/search" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}" \
    -d '{"lat":"nisi","lng":"autem","radius":9}'

```

```javascript
const url = new URL(
    "https://weyounite.dev/api/projects/search"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

let body = {
    "lat": "nisi",
    "lng": "autem",
    "radius": 9
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://weyounite.dev/api/projects/search',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
            'Authorization' => 'Bearer {token}',
        ],
        'json' => [
            'lat' => 'nisi',
            'lng' => 'autem',
            'radius' => 9,
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`POST api/projects/search`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `lat` | decimal |  required  | 
        `lng` | decimal |  required  | 
        `radius` | integer |  required  | 
    
<!-- END_dd06cfd8ba2a7245555c311ab39a6f11 -->

<!-- START_893ae955e8991ef06f6de91adbff0aaa -->
## Get projects

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "https://weyounite.dev/api/projects" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "https://weyounite.dev/api/projects"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://weyounite.dev/api/projects',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
            'Authorization' => 'Bearer {token}',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/projects`


<!-- END_893ae955e8991ef06f6de91adbff0aaa -->

<!-- START_d1a366aa47ee59c96780bfe89ca95bdd -->
## Create a project

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "https://weyounite.dev/api/projects" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}" \
    -d '{"title":"hic","description":"maxime","address":{"lat":50.0989809,"lng":52.98786799,"street":"Hauptstrasse","street_number":"45a","zip":"08678","city":"Buxtehude"}}'

```

```javascript
const url = new URL(
    "https://weyounite.dev/api/projects"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

let body = {
    "title": "hic",
    "description": "maxime",
    "address": {
        "lat": 50.0989809,
        "lng": 52.98786799,
        "street": "Hauptstrasse",
        "street_number": "45a",
        "zip": "08678",
        "city": "Buxtehude"
    }
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://weyounite.dev/api/projects',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
            'Authorization' => 'Bearer {token}',
        ],
        'json' => [
            'title' => 'hic',
            'description' => 'maxime',
            'address' => [
                'lat' => 50.0989809,
                'lng' => 52.98786799,
                'street' => 'Hauptstrasse',
                'street_number' => '45a',
                'zip' => '08678',
                'city' => 'Buxtehude',
            ],
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`POST api/projects`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `title` | string |  required  | 
        `description` | string |  required  | 
        `address.lat` | float |  optional  | 
        `address.lng` | float |  optional  | 
        `address.street` | string |  required  | max 255
        `address.street_number` | string |  required  | max 50
        `address.zip` | string |  required  | max 50
        `address.city` | string |  required  | max 150
    
<!-- END_d1a366aa47ee59c96780bfe89ca95bdd -->

<!-- START_62d96e2c27434ddb7c604817f783bed8 -->
## Get a specific project

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "https://weyounite.dev/api/projects/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "https://weyounite.dev/api/projects/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://weyounite.dev/api/projects/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
            'Authorization' => 'Bearer {token}',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/projects/{project}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `id` |  required  | ID of the resource you want to show

<!-- END_62d96e2c27434ddb7c604817f783bed8 -->

<!-- START_1ca24c8d80ca22395fc07995d7929c34 -->
## Update a project

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X PUT \
    "https://weyounite.dev/api/projects/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}" \
    -d '{"title":"laudantium","description":"quisquam","address":{"lat":50.0989809,"lng":52.98786799,"street":"Hauptstrasse","street_number":"45a","zip":"08678","city":"Buxtehude"}}'

```

```javascript
const url = new URL(
    "https://weyounite.dev/api/projects/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

let body = {
    "title": "laudantium",
    "description": "quisquam",
    "address": {
        "lat": 50.0989809,
        "lng": 52.98786799,
        "street": "Hauptstrasse",
        "street_number": "45a",
        "zip": "08678",
        "city": "Buxtehude"
    }
}

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->put(
    'https://weyounite.dev/api/projects/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
            'Authorization' => 'Bearer {token}',
        ],
        'json' => [
            'title' => 'laudantium',
            'description' => 'quisquam',
            'address' => [
                'lat' => 50.0989809,
                'lng' => 52.98786799,
                'street' => 'Hauptstrasse',
                'street_number' => '45a',
                'zip' => '08678',
                'city' => 'Buxtehude',
            ],
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`PUT api/projects/{project}`

`PATCH api/projects/{project}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `id` |  required  | ID of the resource you want to update
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `title` | string |  required  | 
        `description` | string |  required  | 
        `address.lat` | float |  optional  | 
        `address.lng` | float |  optional  | 
        `address.street` | string |  required  | max 255
        `address.street_number` | string |  required  | max 50
        `address.zip` | string |  required  | max 50
        `address.city` | string |  required  | max 150
    
<!-- END_1ca24c8d80ca22395fc07995d7929c34 -->

<!-- START_70c859bdcb978e6cdba659235c2083d3 -->
## Delete a project

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X DELETE \
    "https://weyounite.dev/api/projects/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "https://weyounite.dev/api/projects/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->delete(
    'https://weyounite.dev/api/projects/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
            'Authorization' => 'Bearer {token}',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`DELETE api/projects/{project}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `id` |  required  | ID of the resource you want to delete

<!-- END_70c859bdcb978e6cdba659235c2083d3 -->

<!-- START_ad8364edcb9cda916e656afaac7ca0cb -->
## Upload a file to a project

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "https://weyounite.dev/api/projects/upload/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}" \
    -d '{"file":"ea"}'

```

```javascript
const url = new URL(
    "https://weyounite.dev/api/projects/upload/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

let body = {
    "file": "ea"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://weyounite.dev/api/projects/upload/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
            'Authorization' => 'Bearer {token}',
        ],
        'json' => [
            'file' => 'ea',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`POST api/projects/upload/{project}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `id` |  optional  | int required ID of the resource where you want to upload a file
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `file` | required |  optional  | A file you want to upload
    
<!-- END_ad8364edcb9cda916e656afaac7ca0cb -->

#SupportedProjects


<!-- START_054c0b1fe654b7398149a5c360b7d7cd -->
## Get the projects supported

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "https://weyounite.dev/api/projects_supported" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "https://weyounite.dev/api/projects_supported"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://weyounite.dev/api/projects_supported',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
            'Authorization' => 'Bearer {token}',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/projects_supported`


<!-- END_054c0b1fe654b7398149a5c360b7d7cd -->

<!-- START_a4a57018eeccc000a5330dd19bbeb53f -->
## Support a project

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "https://weyounite.dev/api/projects_supported" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}" \
    -d '{"user_id":6,"project_id":20}'

```

```javascript
const url = new URL(
    "https://weyounite.dev/api/projects_supported"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

let body = {
    "user_id": 6,
    "project_id": 20
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://weyounite.dev/api/projects_supported',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
            'Authorization' => 'Bearer {token}',
        ],
        'json' => [
            'user_id' => 6,
            'project_id' => 20,
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`POST api/projects_supported`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `user_id` | integer |  required  | ID of the supporter
        `project_id` | integer |  required  | ID of the project
    
<!-- END_a4a57018eeccc000a5330dd19bbeb53f -->

<!-- START_9e45d803b779ea36a0066014e8bef978 -->
## Show a specific support

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "https://weyounite.dev/api/projects_supported/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "https://weyounite.dev/api/projects_supported/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://weyounite.dev/api/projects_supported/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
            'Authorization' => 'Bearer {token}',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/projects_supported/{projects_supported}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `id` |  required  | ID of the resource you want to fetch

<!-- END_9e45d803b779ea36a0066014e8bef978 -->

<!-- START_c9fd14402856842e5b972b724bd1cca2 -->
## Update a support

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X PUT \
    "https://weyounite.dev/api/projects_supported/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}" \
    -d '{"user_id":16,"project_id":20}'

```

```javascript
const url = new URL(
    "https://weyounite.dev/api/projects_supported/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

let body = {
    "user_id": 16,
    "project_id": 20
}

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->put(
    'https://weyounite.dev/api/projects_supported/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
            'Authorization' => 'Bearer {token}',
        ],
        'json' => [
            'user_id' => 16,
            'project_id' => 20,
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`PUT api/projects_supported/{projects_supported}`

`PATCH api/projects_supported/{projects_supported}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `id` |  required  | ID of the resource you want to update
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `user_id` | integer |  required  | ID of the supporter
        `project_id` | integer |  required  | ID of the project
    
<!-- END_c9fd14402856842e5b972b724bd1cca2 -->

<!-- START_ab803ebb1390863458d42b2ba66d769f -->
## Delete a support

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X DELETE \
    "https://weyounite.dev/api/projects_supported/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "https://weyounite.dev/api/projects_supported/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->delete(
    'https://weyounite.dev/api/projects_supported/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
            'Authorization' => 'Bearer {token}',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`DELETE api/projects_supported/{projects_supported}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `id` |  optional  | int required ID of the resource you want to delete

<!-- END_ab803ebb1390863458d42b2ba66d769f -->

#UserManagement


<!-- START_fc1e4f6a697e3c48257de845299b71d5 -->
## Get User

> Example request:

```bash
curl -X GET \
    -G "https://weyounite.dev/api/users" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "https://weyounite.dev/api/users"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://weyounite.dev/api/users',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
            'Authorization' => 'Bearer {token}',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/users`


<!-- END_fc1e4f6a697e3c48257de845299b71d5 -->

<!-- START_8653614346cb0e3d444d164579a0a0a2 -->
## Get a specific user

> Example request:

```bash
curl -X GET \
    -G "https://weyounite.dev/api/users/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "https://weyounite.dev/api/users/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://weyounite.dev/api/users/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
            'Authorization' => 'Bearer {token}',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/users/{user}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `id` |  optional  | int required ID of the resource you want to fetch

<!-- END_8653614346cb0e3d444d164579a0a0a2 -->

<!-- START_d2db7a9fe3abd141d5adbc367a88e969 -->
## Delete an user

> Example request:

```bash
curl -X DELETE \
    "https://weyounite.dev/api/users/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "https://weyounite.dev/api/users/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->delete(
    'https://weyounite.dev/api/users/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
            'Authorization' => 'Bearer {token}',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`DELETE api/users/{user}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `id` |  required  | ID of the resource you want to delete

<!-- END_d2db7a9fe3abd141d5adbc367a88e969 -->

<!-- START_594aa1261378686359a7b4f03517d57b -->
## Upload file for the user

> Example request:

```bash
curl -X POST \
    "https://weyounite.dev/api/users/upload/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}" \
    -d '{"file":"inventore"}'

```

```javascript
const url = new URL(
    "https://weyounite.dev/api/users/upload/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

let body = {
    "file": "inventore"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://weyounite.dev/api/users/upload/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
            'Authorization' => 'Bearer {token}',
        ],
        'json' => [
            'file' => 'inventore',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`POST api/users/upload/{user}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `id` |  required  | ID of the resource where you want to upload a file
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `file` | required |  optional  | A file you want to upload
    
<!-- END_594aa1261378686359a7b4f03517d57b -->


