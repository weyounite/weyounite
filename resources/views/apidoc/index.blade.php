<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>API Reference</title>

    <link rel="stylesheet" href="/docs/css/style.css" />
    <script src="/docs/js/all.js"></script>


          <script>
        $(function() {
            setupLanguages(["bash","javascript","php"]);
        });
      </script>
      </head>

  <body class="">
    <a href="#" id="nav-button">
      <span>
        NAV
        <img src="/docs/images/navbar.png" />
      </span>
    </a>
    <div class="tocify-wrapper">
        <img src="/docs/images/logo.png" />
                    <div class="lang-selector">
                                  <a href="#" data-language-name="bash">bash</a>
                                  <a href="#" data-language-name="javascript">javascript</a>
                                  <a href="#" data-language-name="php">php</a>
                            </div>
                            <div class="search">
              <input type="text" class="search" id="input-search" placeholder="Search">
            </div>
            <ul class="search-results"></ul>
              <div id="toc">
      </div>
                    <ul class="toc-footer">
                                  <li><a href='http://github.com/mpociot/documentarian'>Documentation Powered by Documentarian</a></li>
                            </ul>
            </div>
    <div class="page-wrapper">
      <div class="dark-box"></div>
      <div class="content">
          <!-- START_INFO -->
<h1>Info</h1>
<p>Welcome to the generated API reference.
<a href="{{ route("apidoc.json") }}">Get Postman Collection</a></p>
<!-- END_INFO -->
<h1>Auth</h1>
<!-- START_7b9ab09012dc67331af4f642debe17a0 -->
<h2>Get a bearer token to authenticate</h2>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X POST \
    "https://weyounite.dev/api/auth/token" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}" \
    -d '{"email":"ipsum","password":"ipsam","device_name":"perspiciatis"}'
</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "https://weyounite.dev/api/auth/token"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

let body = {
    "email": "ipsum",
    "password": "ipsam",
    "device_name": "perspiciatis"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response =&gt; response.json())
    .then(json =&gt; console.log(json));</code></pre>
<pre><code class="language-php">
$client = new \GuzzleHttp\Client();
$response = $client-&gt;post(
    'https://weyounite.dev/api/auth/token',
    [
        'headers' =&gt; [
            'Content-Type' =&gt; 'application/json',
            'Accept' =&gt; 'application/json',
            'Authorization' =&gt; 'Bearer {token}',
        ],
        'json' =&gt; [
            'email' =&gt; 'ipsum',
            'password' =&gt; 'ipsam',
            'device_name' =&gt; 'perspiciatis',
        ],
    ]
);
$body = $response-&gt;getBody();
print_r(json_decode((string) $body));</code></pre>
<h3>HTTP Request</h3>
<p><code>POST api/auth/token</code></p>
<h4>Body Parameters</h4>
<table>
<thead>
<tr>
<th>Parameter</th>
<th>Type</th>
<th>Status</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr>
<td><code>email</code></td>
<td>string</td>
<td>required</td>
</tr>
<tr>
<td><code>password</code></td>
<td>string</td>
<td>required</td>
</tr>
<tr>
<td><code>device_name</code></td>
<td>string</td>
<td>required</td>
</tr>
</tbody>
</table>
<!-- END_7b9ab09012dc67331af4f642debe17a0 -->
<h1>Companies</h1>
<!-- START_1e6a34851b0689db52677b43727419b5 -->
<h2>Update a company</h2>
<p><br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small></p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X PUT \
    "https://weyounite.dev/api/companies/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}" \
    -d '{"name":"Werkstatt A","established_at":"2019-01-15","stuff_count":5,"description":"Unternehmensbeschreibung hier","challenges":"Meine Herausforderungen sind ....","strengths":"Ich bin gut in ...","person":{"contact":"mymail.mail.net","has_privacy_settings_accepted":false,"has_agb_accepted":true,"description":"das ist meine Beschreibung","first_name":"Daniel","last_name":"Koch"},"address":{"lat":50.0989809,"lng":52.98786799,"street":"Hauptstrasse","street_number":"45a","zip":"08678","city":"Buxtehude"}}'
</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "https://weyounite.dev/api/companies/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

let body = {
    "name": "Werkstatt A",
    "established_at": "2019-01-15",
    "stuff_count": 5,
    "description": "Unternehmensbeschreibung hier",
    "challenges": "Meine Herausforderungen sind ....",
    "strengths": "Ich bin gut in ...",
    "person": {
        "contact": "mymail.mail.net",
        "has_privacy_settings_accepted": false,
        "has_agb_accepted": true,
        "description": "das ist meine Beschreibung",
        "first_name": "Daniel",
        "last_name": "Koch"
    },
    "address": {
        "lat": 50.0989809,
        "lng": 52.98786799,
        "street": "Hauptstrasse",
        "street_number": "45a",
        "zip": "08678",
        "city": "Buxtehude"
    }
}

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response =&gt; response.json())
    .then(json =&gt; console.log(json));</code></pre>
<pre><code class="language-php">
$client = new \GuzzleHttp\Client();
$response = $client-&gt;put(
    'https://weyounite.dev/api/companies/1',
    [
        'headers' =&gt; [
            'Content-Type' =&gt; 'application/json',
            'Accept' =&gt; 'application/json',
            'Authorization' =&gt; 'Bearer {token}',
        ],
        'json' =&gt; [
            'name' =&gt; 'Werkstatt A',
            'established_at' =&gt; '2019-01-15',
            'stuff_count' =&gt; 5,
            'description' =&gt; 'Unternehmensbeschreibung hier',
            'challenges' =&gt; 'Meine Herausforderungen sind ....',
            'strengths' =&gt; 'Ich bin gut in ...',
            'person' =&gt; [
                'contact' =&gt; 'mymail.mail.net',
                'has_privacy_settings_accepted' =&gt; false,
                'has_agb_accepted' =&gt; true,
                'description' =&gt; 'das ist meine Beschreibung',
                'first_name' =&gt; 'Daniel',
                'last_name' =&gt; 'Koch',
            ],
            'address' =&gt; [
                'lat' =&gt; 50.0989809,
                'lng' =&gt; 52.98786799,
                'street' =&gt; 'Hauptstrasse',
                'street_number' =&gt; '45a',
                'zip' =&gt; '08678',
                'city' =&gt; 'Buxtehude',
            ],
        ],
    ]
);
$body = $response-&gt;getBody();
print_r(json_decode((string) $body));</code></pre>
<blockquote>
<p>Example response (200):</p>
</blockquote>
<pre><code class="language-json">{
    "data": {
        "id": 1,
        "name": "Dominic Dickens",
        "email": "rosalind.spencer@example.com",
        "person": {
            "id": 1,
            "user_id": 1,
            "contact": null,
            "description": "voluptate culpa illum optio qui",
            "first_name": "Heath",
            "last_name": "Luettgen",
            "has_privacy_settings_accepted": false,
            "has_agb_accepted": false,
            "privacy_settings_accepted_at": null,
            "agb_accepted_at": null,
            "created_at": "2020-04-04T18:14:48.000000Z",
            "updated_at": "2020-04-04T18:14:48.000000Z",
            "deleted_at": null
        },
        "helper": {
            "address": {
                "id": 1,
                "addressable_id": 1,
                "addressable_type": "App\\Helper",
                "street": "Alba Islands",
                "street_number": "Glens",
                "zip": "42609",
                "city": "Kossborough",
                "lat": "50.27502400",
                "lng": "-85.88578700",
                "created_at": "2020-04-04T18:14:48.000000Z",
                "updated_at": "2020-04-04T18:14:48.000000Z",
                "deleted_at": null
            }
        },
        "company": {
            "established_at": "2020-01-01T00:00:00.000000Z",
            "name": "qui",
            "staff_count": 1,
            "description": "molestiae",
            "challenges": "culpa",
            "strengths": "consectetur",
            "address": {
                "id": 22,
                "addressable_id": 11,
                "addressable_type": "App\\Company",
                "street": "a",
                "street_number": "amet",
                "zip": "sint",
                "city": "cum",
                "lat": null,
                "lng": null,
                "created_at": "2020-04-04T18:40:17.000000Z",
                "updated_at": "2020-04-04T18:40:17.000000Z",
                "deleted_at": null
            }
        },
        "files": []
    }
}</code></pre>
<h3>HTTP Request</h3>
<p><code>PUT api/companies/{company}</code></p>
<p><code>PATCH api/companies/{company}</code></p>
<h4>URL Parameters</h4>
<table>
<thead>
<tr>
<th>Parameter</th>
<th>Status</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr>
<td><code>id</code></td>
<td>optional</td>
<td>int required ID of the company</td>
</tr>
</tbody>
</table>
<h4>Body Parameters</h4>
<table>
<thead>
<tr>
<th>Parameter</th>
<th>Type</th>
<th>Status</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr>
<td><code>name</code></td>
<td>string</td>
<td>required</td>
</tr>
<tr>
<td><code>established_at</code></td>
<td>date</td>
<td>optional</td>
</tr>
<tr>
<td><code>stuff_count</code></td>
<td>integer</td>
<td>optional</td>
</tr>
<tr>
<td><code>description</code></td>
<td>string</td>
<td>optional</td>
</tr>
<tr>
<td><code>challenges</code></td>
<td>string</td>
<td>optional</td>
</tr>
<tr>
<td><code>strengths</code></td>
<td>string</td>
<td>optional</td>
</tr>
<tr>
<td><code>person.contact</code></td>
<td>string</td>
<td>optional</td>
</tr>
<tr>
<td><code>person.has_privacy_settings_accepted</code></td>
<td>boolean</td>
<td>required</td>
</tr>
<tr>
<td><code>person.has_agb_accepted</code></td>
<td>boolean</td>
<td>required</td>
</tr>
<tr>
<td><code>person.description</code></td>
<td>string</td>
<td>optional</td>
</tr>
<tr>
<td><code>person.first_name</code></td>
<td>string</td>
<td>optional</td>
</tr>
<tr>
<td><code>person.last_name</code></td>
<td>string</td>
<td>optional</td>
</tr>
<tr>
<td><code>address.lat</code></td>
<td>float</td>
<td>optional</td>
</tr>
<tr>
<td><code>address.lng</code></td>
<td>float</td>
<td>optional</td>
</tr>
<tr>
<td><code>address.street</code></td>
<td>string</td>
<td>required</td>
<td>max 255</td>
</tr>
<tr>
<td><code>address.street_number</code></td>
<td>string</td>
<td>required</td>
<td>max 50</td>
</tr>
<tr>
<td><code>address.zip</code></td>
<td>string</td>
<td>required</td>
<td>max 50</td>
</tr>
<tr>
<td><code>address.city</code></td>
<td>string</td>
<td>required</td>
<td>max 150</td>
</tr>
</tbody>
</table>
<!-- END_1e6a34851b0689db52677b43727419b5 -->
<!-- START_72de66eabebc78e1d0e514081409da3a -->
<h2>Delete a company</h2>
<p><br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small></p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X DELETE \
    "https://weyounite.dev/api/companies/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "https://weyounite.dev/api/companies/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response =&gt; response.json())
    .then(json =&gt; console.log(json));</code></pre>
<pre><code class="language-php">
$client = new \GuzzleHttp\Client();
$response = $client-&gt;delete(
    'https://weyounite.dev/api/companies/1',
    [
        'headers' =&gt; [
            'Content-Type' =&gt; 'application/json',
            'Accept' =&gt; 'application/json',
            'Authorization' =&gt; 'Bearer {token}',
        ],
    ]
);
$body = $response-&gt;getBody();
print_r(json_decode((string) $body));</code></pre>
<h3>HTTP Request</h3>
<p><code>DELETE api/companies/{company}</code></p>
<!-- END_72de66eabebc78e1d0e514081409da3a -->
<!-- START_4bc61bb957ffc4533cf83fc0d656fc9f -->
<h2>Create a company</h2>
<p><br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small></p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X POST \
    "https://weyounite.dev/api/companies/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}" \
    -d '{"name":"Werkstatt A","established_at":"2019-01-15","stuff_count":5,"description":"Unternehmensbeschreibung hier","challenges":"Meine Herausforderungen sind ....","strengths":"Ich bin gut in ...","person":{"contact":"mymail.mail.net","has_privacy_settings_accepted":false,"has_agb_accepted":true,"description":"das ist meine Beschreibung","first_name":"Daniel","last_name":"Koch"},"address":{"lat":50.0989809,"lng":52.98786799,"street":"Hauptstrasse","street_number":"45a","zip":"08678","city":"Buxtehude"}}'
</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "https://weyounite.dev/api/companies/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

let body = {
    "name": "Werkstatt A",
    "established_at": "2019-01-15",
    "stuff_count": 5,
    "description": "Unternehmensbeschreibung hier",
    "challenges": "Meine Herausforderungen sind ....",
    "strengths": "Ich bin gut in ...",
    "person": {
        "contact": "mymail.mail.net",
        "has_privacy_settings_accepted": false,
        "has_agb_accepted": true,
        "description": "das ist meine Beschreibung",
        "first_name": "Daniel",
        "last_name": "Koch"
    },
    "address": {
        "lat": 50.0989809,
        "lng": 52.98786799,
        "street": "Hauptstrasse",
        "street_number": "45a",
        "zip": "08678",
        "city": "Buxtehude"
    }
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response =&gt; response.json())
    .then(json =&gt; console.log(json));</code></pre>
<pre><code class="language-php">
$client = new \GuzzleHttp\Client();
$response = $client-&gt;post(
    'https://weyounite.dev/api/companies/1',
    [
        'headers' =&gt; [
            'Content-Type' =&gt; 'application/json',
            'Accept' =&gt; 'application/json',
            'Authorization' =&gt; 'Bearer {token}',
        ],
        'json' =&gt; [
            'name' =&gt; 'Werkstatt A',
            'established_at' =&gt; '2019-01-15',
            'stuff_count' =&gt; 5,
            'description' =&gt; 'Unternehmensbeschreibung hier',
            'challenges' =&gt; 'Meine Herausforderungen sind ....',
            'strengths' =&gt; 'Ich bin gut in ...',
            'person' =&gt; [
                'contact' =&gt; 'mymail.mail.net',
                'has_privacy_settings_accepted' =&gt; false,
                'has_agb_accepted' =&gt; true,
                'description' =&gt; 'das ist meine Beschreibung',
                'first_name' =&gt; 'Daniel',
                'last_name' =&gt; 'Koch',
            ],
            'address' =&gt; [
                'lat' =&gt; 50.0989809,
                'lng' =&gt; 52.98786799,
                'street' =&gt; 'Hauptstrasse',
                'street_number' =&gt; '45a',
                'zip' =&gt; '08678',
                'city' =&gt; 'Buxtehude',
            ],
        ],
    ]
);
$body = $response-&gt;getBody();
print_r(json_decode((string) $body));</code></pre>
<blockquote>
<p>Example response (200):</p>
</blockquote>
<pre><code class="language-json">{
    "data": {
        "id": 1,
        "name": "Dominic Dickens",
        "email": "rosalind.spencer@example.com",
        "person": {
            "id": 1,
            "user_id": 1,
            "contact": null,
            "description": "voluptate culpa illum optio qui",
            "first_name": "Heath",
            "last_name": "Luettgen",
            "has_privacy_settings_accepted": false,
            "has_agb_accepted": false,
            "privacy_settings_accepted_at": null,
            "agb_accepted_at": null,
            "created_at": "2020-04-04T18:14:48.000000Z",
            "updated_at": "2020-04-04T18:14:48.000000Z",
            "deleted_at": null
        },
        "helper": {
            "address": {
                "id": 1,
                "addressable_id": 1,
                "addressable_type": "App\\Helper",
                "street": "Alba Islands",
                "street_number": "Glens",
                "zip": "42609",
                "city": "Kossborough",
                "lat": "50.27502400",
                "lng": "-85.88578700",
                "created_at": "2020-04-04T18:14:48.000000Z",
                "updated_at": "2020-04-04T18:14:48.000000Z",
                "deleted_at": null
            }
        },
        "company": {
            "established_at": "2020-01-01T00:00:00.000000Z",
            "name": "qui",
            "staff_count": 1,
            "description": "molestiae",
            "challenges": "culpa",
            "strengths": "consectetur",
            "address": {
                "id": 22,
                "addressable_id": 11,
                "addressable_type": "App\\Company",
                "street": "a",
                "street_number": "amet",
                "zip": "sint",
                "city": "cum",
                "lat": null,
                "lng": null,
                "created_at": "2020-04-04T18:40:17.000000Z",
                "updated_at": "2020-04-04T18:40:17.000000Z",
                "deleted_at": null
            }
        },
        "files": []
    }
}</code></pre>
<h3>HTTP Request</h3>
<p><code>POST api/companies/{user}</code></p>
<h4>URL Parameters</h4>
<table>
<thead>
<tr>
<th>Parameter</th>
<th>Status</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr>
<td><code>id</code></td>
<td>optional</td>
<td>int required ID of the user</td>
</tr>
</tbody>
</table>
<h4>Body Parameters</h4>
<table>
<thead>
<tr>
<th>Parameter</th>
<th>Type</th>
<th>Status</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr>
<td><code>name</code></td>
<td>string</td>
<td>required</td>
</tr>
<tr>
<td><code>established_at</code></td>
<td>date</td>
<td>optional</td>
</tr>
<tr>
<td><code>stuff_count</code></td>
<td>integer</td>
<td>optional</td>
</tr>
<tr>
<td><code>description</code></td>
<td>string</td>
<td>optional</td>
</tr>
<tr>
<td><code>challenges</code></td>
<td>string</td>
<td>optional</td>
</tr>
<tr>
<td><code>strengths</code></td>
<td>string</td>
<td>optional</td>
</tr>
<tr>
<td><code>person.contact</code></td>
<td>string</td>
<td>optional</td>
</tr>
<tr>
<td><code>person.has_privacy_settings_accepted</code></td>
<td>boolean</td>
<td>required</td>
</tr>
<tr>
<td><code>person.has_agb_accepted</code></td>
<td>boolean</td>
<td>required</td>
</tr>
<tr>
<td><code>person.description</code></td>
<td>string</td>
<td>optional</td>
</tr>
<tr>
<td><code>person.first_name</code></td>
<td>string</td>
<td>optional</td>
</tr>
<tr>
<td><code>person.last_name</code></td>
<td>string</td>
<td>optional</td>
</tr>
<tr>
<td><code>address.lat</code></td>
<td>float</td>
<td>optional</td>
</tr>
<tr>
<td><code>address.lng</code></td>
<td>float</td>
<td>optional</td>
</tr>
<tr>
<td><code>address.street</code></td>
<td>string</td>
<td>required</td>
<td>max 255</td>
</tr>
<tr>
<td><code>address.street_number</code></td>
<td>string</td>
<td>required</td>
<td>max 50</td>
</tr>
<tr>
<td><code>address.zip</code></td>
<td>string</td>
<td>required</td>
<td>max 50</td>
</tr>
<tr>
<td><code>address.city</code></td>
<td>string</td>
<td>required</td>
<td>max 150</td>
</tr>
</tbody>
</table>
<!-- END_4bc61bb957ffc4533cf83fc0d656fc9f -->
<h1>ContactForm</h1>
<!-- START_d82defe8489483fff70f435f9c68ce2d -->
<h2>Send a contact message to the weyounite slack channel</h2>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X POST \
    "https://weyounite.dev/api/contact/request" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}" \
    -d '{"email":"reprehenderit","concern":"quibusdam"}'
</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "https://weyounite.dev/api/contact/request"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

let body = {
    "email": "reprehenderit",
    "concern": "quibusdam"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response =&gt; response.json())
    .then(json =&gt; console.log(json));</code></pre>
<pre><code class="language-php">
$client = new \GuzzleHttp\Client();
$response = $client-&gt;post(
    'https://weyounite.dev/api/contact/request',
    [
        'headers' =&gt; [
            'Content-Type' =&gt; 'application/json',
            'Accept' =&gt; 'application/json',
            'Authorization' =&gt; 'Bearer {token}',
        ],
        'json' =&gt; [
            'email' =&gt; 'reprehenderit',
            'concern' =&gt; 'quibusdam',
        ],
    ]
);
$body = $response-&gt;getBody();
print_r(json_decode((string) $body));</code></pre>
<h3>HTTP Request</h3>
<p><code>POST api/contact/request</code></p>
<h4>Body Parameters</h4>
<table>
<thead>
<tr>
<th>Parameter</th>
<th>Type</th>
<th>Status</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr>
<td><code>email</code></td>
<td>string</td>
<td>required</td>
</tr>
<tr>
<td><code>concern</code></td>
<td>string</td>
<td>required</td>
<td>send your message</td>
</tr>
</tbody>
</table>
<!-- END_d82defe8489483fff70f435f9c68ce2d -->
<h1>DirectMessages</h1>
<!-- START_c61e9c2b3fdeea56ee207c8db3d88546 -->
<h2>Get messages for an authenticated user</h2>
<p><br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small></p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X GET \
    -G "https://weyounite.dev/api/messages" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "https://weyounite.dev/api/messages"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response =&gt; response.json())
    .then(json =&gt; console.log(json));</code></pre>
<pre><code class="language-php">
$client = new \GuzzleHttp\Client();
$response = $client-&gt;get(
    'https://weyounite.dev/api/messages',
    [
        'headers' =&gt; [
            'Content-Type' =&gt; 'application/json',
            'Accept' =&gt; 'application/json',
            'Authorization' =&gt; 'Bearer {token}',
        ],
    ]
);
$body = $response-&gt;getBody();
print_r(json_decode((string) $body));</code></pre>
<blockquote>
<p>Example response (401):</p>
</blockquote>
<pre><code class="language-json">{
    "message": "Unauthenticated."
}</code></pre>
<h3>HTTP Request</h3>
<p><code>GET api/messages</code></p>
<!-- END_c61e9c2b3fdeea56ee207c8db3d88546 -->
<!-- START_cef8438938cd56a8a7a685a273a52336 -->
<h2>Show a specific message</h2>
<p><br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small></p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X GET \
    -G "https://weyounite.dev/api/messages/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "https://weyounite.dev/api/messages/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response =&gt; response.json())
    .then(json =&gt; console.log(json));</code></pre>
<pre><code class="language-php">
$client = new \GuzzleHttp\Client();
$response = $client-&gt;get(
    'https://weyounite.dev/api/messages/1',
    [
        'headers' =&gt; [
            'Content-Type' =&gt; 'application/json',
            'Accept' =&gt; 'application/json',
            'Authorization' =&gt; 'Bearer {token}',
        ],
    ]
);
$body = $response-&gt;getBody();
print_r(json_decode((string) $body));</code></pre>
<blockquote>
<p>Example response (401):</p>
</blockquote>
<pre><code class="language-json">{
    "message": "Unauthenticated."
}</code></pre>
<h3>HTTP Request</h3>
<p><code>GET api/messages/{message}</code></p>
<h4>URL Parameters</h4>
<table>
<thead>
<tr>
<th>Parameter</th>
<th>Status</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr>
<td><code>id</code></td>
<td>optional</td>
<td>string required ID of the message</td>
</tr>
</tbody>
</table>
<!-- END_cef8438938cd56a8a7a685a273a52336 -->
<!-- START_7b6f93fba506305412683dbf77dd0cc9 -->
<h2>Send a message to a specific user</h2>
<p><br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small></p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X POST \
    "https://weyounite.dev/api/messages/send/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}" \
    -d '{"message":"Die Message die ich heir senden will","replyToId":"deleniti"}'
</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "https://weyounite.dev/api/messages/send/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

let body = {
    "message": "Die Message die ich heir senden will",
    "replyToId": "deleniti"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response =&gt; response.json())
    .then(json =&gt; console.log(json));</code></pre>
<pre><code class="language-php">
$client = new \GuzzleHttp\Client();
$response = $client-&gt;post(
    'https://weyounite.dev/api/messages/send/1',
    [
        'headers' =&gt; [
            'Content-Type' =&gt; 'application/json',
            'Accept' =&gt; 'application/json',
            'Authorization' =&gt; 'Bearer {token}',
        ],
        'json' =&gt; [
            'message' =&gt; 'Die Message die ich heir senden will',
            'replyToId' =&gt; 'deleniti',
        ],
    ]
);
$body = $response-&gt;getBody();
print_r(json_decode((string) $body));</code></pre>
<blockquote>
<p>Example response (200):</p>
</blockquote>
<pre><code class="language-json">{
    "success": true
}</code></pre>
<h3>HTTP Request</h3>
<p><code>POST api/messages/send/{receiver}</code></p>
<h4>URL Parameters</h4>
<table>
<thead>
<tr>
<th>Parameter</th>
<th>Status</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr>
<td><code>id</code></td>
<td>optional</td>
<td>int required ID of the receiver</td>
</tr>
</tbody>
</table>
<h4>Body Parameters</h4>
<table>
<thead>
<tr>
<th>Parameter</th>
<th>Type</th>
<th>Status</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr>
<td><code>message</code></td>
<td>string</td>
<td>required</td>
</tr>
<tr>
<td><code>replyToId</code></td>
<td>string</td>
<td>optional</td>
<td>ID of the message I want to reply if it is a reply</td>
</tr>
</tbody>
</table>
<!-- END_7b6f93fba506305412683dbf77dd0cc9 -->
<!-- START_6ce1f52a38ffa11186d9904e22bb15aa -->
<h2>Mark a message as unread</h2>
<p><br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small></p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X PUT \
    "https://weyounite.dev/api/messages/mark_as_read/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "https://weyounite.dev/api/messages/mark_as_read/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response =&gt; response.json())
    .then(json =&gt; console.log(json));</code></pre>
<pre><code class="language-php">
$client = new \GuzzleHttp\Client();
$response = $client-&gt;put(
    'https://weyounite.dev/api/messages/mark_as_read/1',
    [
        'headers' =&gt; [
            'Content-Type' =&gt; 'application/json',
            'Accept' =&gt; 'application/json',
            'Authorization' =&gt; 'Bearer {token}',
        ],
    ]
);
$body = $response-&gt;getBody();
print_r(json_decode((string) $body));</code></pre>
<h3>HTTP Request</h3>
<p><code>PUT api/messages/mark_as_read/{message}</code></p>
<h4>URL Parameters</h4>
<table>
<thead>
<tr>
<th>Parameter</th>
<th>Status</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr>
<td><code>id</code></td>
<td>optional</td>
<td>string required ID of the message</td>
</tr>
</tbody>
</table>
<!-- END_6ce1f52a38ffa11186d9904e22bb15aa -->
<h1>Helper</h1>
<!-- START_f8b315cf38f34c37425f3116002b6858 -->
<h2>Create a helper</h2>
<p><br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small></p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X PUT \
    "https://weyounite.dev/api/helper/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}" \
    -d '{"person":{"contact":"mymail.mail.net","has_privacy_settings_accepted":false,"has_agb_accepted":true,"description":"das ist meine Beschreibung","first_name":"Daniel","last_name":"Koch"},"address":{"lat":50.0989809,"lng":52.98786799,"street":"Hauptstrasse","street_number":"45a","zip":"08678","city":"Buxtehude"}}'
</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "https://weyounite.dev/api/helper/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

let body = {
    "person": {
        "contact": "mymail.mail.net",
        "has_privacy_settings_accepted": false,
        "has_agb_accepted": true,
        "description": "das ist meine Beschreibung",
        "first_name": "Daniel",
        "last_name": "Koch"
    },
    "address": {
        "lat": 50.0989809,
        "lng": 52.98786799,
        "street": "Hauptstrasse",
        "street_number": "45a",
        "zip": "08678",
        "city": "Buxtehude"
    }
}

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response =&gt; response.json())
    .then(json =&gt; console.log(json));</code></pre>
<pre><code class="language-php">
$client = new \GuzzleHttp\Client();
$response = $client-&gt;put(
    'https://weyounite.dev/api/helper/1',
    [
        'headers' =&gt; [
            'Content-Type' =&gt; 'application/json',
            'Accept' =&gt; 'application/json',
            'Authorization' =&gt; 'Bearer {token}',
        ],
        'json' =&gt; [
            'person' =&gt; [
                'contact' =&gt; 'mymail.mail.net',
                'has_privacy_settings_accepted' =&gt; false,
                'has_agb_accepted' =&gt; true,
                'description' =&gt; 'das ist meine Beschreibung',
                'first_name' =&gt; 'Daniel',
                'last_name' =&gt; 'Koch',
            ],
            'address' =&gt; [
                'lat' =&gt; 50.0989809,
                'lng' =&gt; 52.98786799,
                'street' =&gt; 'Hauptstrasse',
                'street_number' =&gt; '45a',
                'zip' =&gt; '08678',
                'city' =&gt; 'Buxtehude',
            ],
        ],
    ]
);
$body = $response-&gt;getBody();
print_r(json_decode((string) $body));</code></pre>
<blockquote>
<p>Example response (200):</p>
</blockquote>
<pre><code class="language-json">{
    "data": {
        "id": 1,
        "name": "Dominic Dickens",
        "email": "rosalind.spencer@example.com",
        "person": {
            "id": 1,
            "user_id": 1,
            "contact": null,
            "description": "voluptate culpa illum optio qui",
            "first_name": "Heath",
            "last_name": "Luettgen",
            "has_privacy_settings_accepted": false,
            "has_agb_accepted": false,
            "privacy_settings_accepted_at": null,
            "agb_accepted_at": null,
            "created_at": "2020-04-04T18:14:48.000000Z",
            "updated_at": "2020-04-04T18:14:48.000000Z",
            "deleted_at": null
        },
        "helper": {
            "address": {
                "id": 1,
                "addressable_id": 1,
                "addressable_type": "App\\Helper",
                "street": "Alba Islands",
                "street_number": "Glens",
                "zip": "42609",
                "city": "Kossborough",
                "lat": "50.27502400",
                "lng": "-85.88578700",
                "created_at": "2020-04-04T18:14:48.000000Z",
                "updated_at": "2020-04-04T18:14:48.000000Z",
                "deleted_at": null
            }
        },
        "company": {
            "established_at": "2020-01-01T00:00:00.000000Z",
            "name": "qui",
            "staff_count": 1,
            "description": "molestiae",
            "challenges": "culpa",
            "strengths": "consectetur",
            "address": {
                "id": 22,
                "addressable_id": 11,
                "addressable_type": "App\\Company",
                "street": "a",
                "street_number": "amet",
                "zip": "sint",
                "city": "cum",
                "lat": null,
                "lng": null,
                "created_at": "2020-04-04T18:40:17.000000Z",
                "updated_at": "2020-04-04T18:40:17.000000Z",
                "deleted_at": null
            }
        },
        "files": []
    }
}</code></pre>
<h3>HTTP Request</h3>
<p><code>PUT api/helper/{helper}</code></p>
<p><code>PATCH api/helper/{helper}</code></p>
<h4>URL Parameters</h4>
<table>
<thead>
<tr>
<th>Parameter</th>
<th>Status</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr>
<td><code>id</code></td>
<td>optional</td>
<td>int required ID of the user</td>
</tr>
</tbody>
</table>
<h4>Body Parameters</h4>
<table>
<thead>
<tr>
<th>Parameter</th>
<th>Type</th>
<th>Status</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr>
<td><code>person.contact</code></td>
<td>string</td>
<td>optional</td>
</tr>
<tr>
<td><code>person.has_privacy_settings_accepted</code></td>
<td>boolean</td>
<td>required</td>
</tr>
<tr>
<td><code>person.has_agb_accepted</code></td>
<td>boolean</td>
<td>required</td>
</tr>
<tr>
<td><code>person.description</code></td>
<td>string</td>
<td>optional</td>
</tr>
<tr>
<td><code>person.first_name</code></td>
<td>string</td>
<td>optional</td>
</tr>
<tr>
<td><code>person.last_name</code></td>
<td>string</td>
<td>optional</td>
</tr>
<tr>
<td><code>address.lat</code></td>
<td>float</td>
<td>optional</td>
</tr>
<tr>
<td><code>address.lng</code></td>
<td>float</td>
<td>optional</td>
</tr>
<tr>
<td><code>address.street</code></td>
<td>string</td>
<td>required</td>
<td>max 255</td>
</tr>
<tr>
<td><code>address.street_number</code></td>
<td>string</td>
<td>required</td>
<td>max 50</td>
</tr>
<tr>
<td><code>address.zip</code></td>
<td>string</td>
<td>required</td>
<td>max 50</td>
</tr>
<tr>
<td><code>address.city</code></td>
<td>string</td>
<td>required</td>
<td>max 150</td>
</tr>
</tbody>
</table>
<!-- END_f8b315cf38f34c37425f3116002b6858 -->
<!-- START_1991dcd161689f359a42ce3f5f621125 -->
<h2>Delete a helper</h2>
<p><br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small></p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X DELETE \
    "https://weyounite.dev/api/helper/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "https://weyounite.dev/api/helper/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response =&gt; response.json())
    .then(json =&gt; console.log(json));</code></pre>
<pre><code class="language-php">
$client = new \GuzzleHttp\Client();
$response = $client-&gt;delete(
    'https://weyounite.dev/api/helper/1',
    [
        'headers' =&gt; [
            'Content-Type' =&gt; 'application/json',
            'Accept' =&gt; 'application/json',
            'Authorization' =&gt; 'Bearer {token}',
        ],
    ]
);
$body = $response-&gt;getBody();
print_r(json_decode((string) $body));</code></pre>
<h3>HTTP Request</h3>
<p><code>DELETE api/helper/{helper}</code></p>
<!-- END_1991dcd161689f359a42ce3f5f621125 -->
<!-- START_4ba978be325a8c0db81a652edffb127a -->
<h2>Create a helper</h2>
<p><br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small></p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X POST \
    "https://weyounite.dev/api/helper/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}" \
    -d '{"person":{"contact":"mymail.mail.net","has_privacy_settings_accepted":false,"has_agb_accepted":true,"description":"das ist meine Beschreibung","first_name":"Daniel","last_name":"Koch"},"address":{"lat":50.0989809,"lng":52.98786799,"street":"Hauptstrasse","street_number":"45a","zip":"08678","city":"Buxtehude"}}'
</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "https://weyounite.dev/api/helper/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

let body = {
    "person": {
        "contact": "mymail.mail.net",
        "has_privacy_settings_accepted": false,
        "has_agb_accepted": true,
        "description": "das ist meine Beschreibung",
        "first_name": "Daniel",
        "last_name": "Koch"
    },
    "address": {
        "lat": 50.0989809,
        "lng": 52.98786799,
        "street": "Hauptstrasse",
        "street_number": "45a",
        "zip": "08678",
        "city": "Buxtehude"
    }
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response =&gt; response.json())
    .then(json =&gt; console.log(json));</code></pre>
<pre><code class="language-php">
$client = new \GuzzleHttp\Client();
$response = $client-&gt;post(
    'https://weyounite.dev/api/helper/1',
    [
        'headers' =&gt; [
            'Content-Type' =&gt; 'application/json',
            'Accept' =&gt; 'application/json',
            'Authorization' =&gt; 'Bearer {token}',
        ],
        'json' =&gt; [
            'person' =&gt; [
                'contact' =&gt; 'mymail.mail.net',
                'has_privacy_settings_accepted' =&gt; false,
                'has_agb_accepted' =&gt; true,
                'description' =&gt; 'das ist meine Beschreibung',
                'first_name' =&gt; 'Daniel',
                'last_name' =&gt; 'Koch',
            ],
            'address' =&gt; [
                'lat' =&gt; 50.0989809,
                'lng' =&gt; 52.98786799,
                'street' =&gt; 'Hauptstrasse',
                'street_number' =&gt; '45a',
                'zip' =&gt; '08678',
                'city' =&gt; 'Buxtehude',
            ],
        ],
    ]
);
$body = $response-&gt;getBody();
print_r(json_decode((string) $body));</code></pre>
<blockquote>
<p>Example response (200):</p>
</blockquote>
<pre><code class="language-json">{
    "data": {
        "id": 1,
        "name": "Dominic Dickens",
        "email": "rosalind.spencer@example.com",
        "person": {
            "id": 1,
            "user_id": 1,
            "contact": null,
            "description": "voluptate culpa illum optio qui",
            "first_name": "Heath",
            "last_name": "Luettgen",
            "has_privacy_settings_accepted": false,
            "has_agb_accepted": false,
            "privacy_settings_accepted_at": null,
            "agb_accepted_at": null,
            "created_at": "2020-04-04T18:14:48.000000Z",
            "updated_at": "2020-04-04T18:14:48.000000Z",
            "deleted_at": null
        },
        "helper": {
            "address": {
                "id": 1,
                "addressable_id": 1,
                "addressable_type": "App\\Helper",
                "street": "Alba Islands",
                "street_number": "Glens",
                "zip": "42609",
                "city": "Kossborough",
                "lat": "50.27502400",
                "lng": "-85.88578700",
                "created_at": "2020-04-04T18:14:48.000000Z",
                "updated_at": "2020-04-04T18:14:48.000000Z",
                "deleted_at": null
            }
        },
        "company": {
            "established_at": "2020-01-01T00:00:00.000000Z",
            "name": "qui",
            "staff_count": 1,
            "description": "molestiae",
            "challenges": "culpa",
            "strengths": "consectetur",
            "address": {
                "id": 22,
                "addressable_id": 11,
                "addressable_type": "App\\Company",
                "street": "a",
                "street_number": "amet",
                "zip": "sint",
                "city": "cum",
                "lat": null,
                "lng": null,
                "created_at": "2020-04-04T18:40:17.000000Z",
                "updated_at": "2020-04-04T18:40:17.000000Z",
                "deleted_at": null
            }
        },
        "files": []
    }
}</code></pre>
<h3>HTTP Request</h3>
<p><code>POST api/helper/{user}</code></p>
<h4>URL Parameters</h4>
<table>
<thead>
<tr>
<th>Parameter</th>
<th>Status</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr>
<td><code>id</code></td>
<td>optional</td>
<td>int required ID of the user</td>
</tr>
</tbody>
</table>
<h4>Body Parameters</h4>
<table>
<thead>
<tr>
<th>Parameter</th>
<th>Type</th>
<th>Status</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr>
<td><code>person.contact</code></td>
<td>string</td>
<td>optional</td>
</tr>
<tr>
<td><code>person.has_privacy_settings_accepted</code></td>
<td>boolean</td>
<td>required</td>
</tr>
<tr>
<td><code>person.has_agb_accepted</code></td>
<td>boolean</td>
<td>required</td>
</tr>
<tr>
<td><code>person.description</code></td>
<td>string</td>
<td>optional</td>
</tr>
<tr>
<td><code>person.first_name</code></td>
<td>string</td>
<td>optional</td>
</tr>
<tr>
<td><code>person.last_name</code></td>
<td>string</td>
<td>optional</td>
</tr>
<tr>
<td><code>address.lat</code></td>
<td>float</td>
<td>optional</td>
</tr>
<tr>
<td><code>address.lng</code></td>
<td>float</td>
<td>optional</td>
</tr>
<tr>
<td><code>address.street</code></td>
<td>string</td>
<td>required</td>
<td>max 255</td>
</tr>
<tr>
<td><code>address.street_number</code></td>
<td>string</td>
<td>required</td>
<td>max 50</td>
</tr>
<tr>
<td><code>address.zip</code></td>
<td>string</td>
<td>required</td>
<td>max 50</td>
</tr>
<tr>
<td><code>address.city</code></td>
<td>string</td>
<td>required</td>
<td>max 150</td>
</tr>
</tbody>
</table>
<!-- END_4ba978be325a8c0db81a652edffb127a -->
<h1>Projects</h1>
<!-- START_dd06cfd8ba2a7245555c311ab39a6f11 -->
<h2>Search Projects</h2>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X POST \
    "https://weyounite.dev/api/projects/search" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}" \
    -d '{"lat":"nisi","lng":"autem","radius":9}'
</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "https://weyounite.dev/api/projects/search"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

let body = {
    "lat": "nisi",
    "lng": "autem",
    "radius": 9
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response =&gt; response.json())
    .then(json =&gt; console.log(json));</code></pre>
<pre><code class="language-php">
$client = new \GuzzleHttp\Client();
$response = $client-&gt;post(
    'https://weyounite.dev/api/projects/search',
    [
        'headers' =&gt; [
            'Content-Type' =&gt; 'application/json',
            'Accept' =&gt; 'application/json',
            'Authorization' =&gt; 'Bearer {token}',
        ],
        'json' =&gt; [
            'lat' =&gt; 'nisi',
            'lng' =&gt; 'autem',
            'radius' =&gt; 9,
        ],
    ]
);
$body = $response-&gt;getBody();
print_r(json_decode((string) $body));</code></pre>
<h3>HTTP Request</h3>
<p><code>POST api/projects/search</code></p>
<h4>Body Parameters</h4>
<table>
<thead>
<tr>
<th>Parameter</th>
<th>Type</th>
<th>Status</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr>
<td><code>lat</code></td>
<td>decimal</td>
<td>required</td>
</tr>
<tr>
<td><code>lng</code></td>
<td>decimal</td>
<td>required</td>
</tr>
<tr>
<td><code>radius</code></td>
<td>integer</td>
<td>required</td>
</tr>
</tbody>
</table>
<!-- END_dd06cfd8ba2a7245555c311ab39a6f11 -->
<!-- START_893ae955e8991ef06f6de91adbff0aaa -->
<h2>Get projects</h2>
<p><br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small></p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X GET \
    -G "https://weyounite.dev/api/projects" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "https://weyounite.dev/api/projects"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response =&gt; response.json())
    .then(json =&gt; console.log(json));</code></pre>
<pre><code class="language-php">
$client = new \GuzzleHttp\Client();
$response = $client-&gt;get(
    'https://weyounite.dev/api/projects',
    [
        'headers' =&gt; [
            'Content-Type' =&gt; 'application/json',
            'Accept' =&gt; 'application/json',
            'Authorization' =&gt; 'Bearer {token}',
        ],
    ]
);
$body = $response-&gt;getBody();
print_r(json_decode((string) $body));</code></pre>
<blockquote>
<p>Example response (401):</p>
</blockquote>
<pre><code class="language-json">{
    "message": "Unauthenticated."
}</code></pre>
<h3>HTTP Request</h3>
<p><code>GET api/projects</code></p>
<!-- END_893ae955e8991ef06f6de91adbff0aaa -->
<!-- START_d1a366aa47ee59c96780bfe89ca95bdd -->
<h2>Create a project</h2>
<p><br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small></p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X POST \
    "https://weyounite.dev/api/projects" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}" \
    -d '{"title":"hic","description":"maxime","address":{"lat":50.0989809,"lng":52.98786799,"street":"Hauptstrasse","street_number":"45a","zip":"08678","city":"Buxtehude"}}'
</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "https://weyounite.dev/api/projects"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

let body = {
    "title": "hic",
    "description": "maxime",
    "address": {
        "lat": 50.0989809,
        "lng": 52.98786799,
        "street": "Hauptstrasse",
        "street_number": "45a",
        "zip": "08678",
        "city": "Buxtehude"
    }
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response =&gt; response.json())
    .then(json =&gt; console.log(json));</code></pre>
<pre><code class="language-php">
$client = new \GuzzleHttp\Client();
$response = $client-&gt;post(
    'https://weyounite.dev/api/projects',
    [
        'headers' =&gt; [
            'Content-Type' =&gt; 'application/json',
            'Accept' =&gt; 'application/json',
            'Authorization' =&gt; 'Bearer {token}',
        ],
        'json' =&gt; [
            'title' =&gt; 'hic',
            'description' =&gt; 'maxime',
            'address' =&gt; [
                'lat' =&gt; 50.0989809,
                'lng' =&gt; 52.98786799,
                'street' =&gt; 'Hauptstrasse',
                'street_number' =&gt; '45a',
                'zip' =&gt; '08678',
                'city' =&gt; 'Buxtehude',
            ],
        ],
    ]
);
$body = $response-&gt;getBody();
print_r(json_decode((string) $body));</code></pre>
<h3>HTTP Request</h3>
<p><code>POST api/projects</code></p>
<h4>Body Parameters</h4>
<table>
<thead>
<tr>
<th>Parameter</th>
<th>Type</th>
<th>Status</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr>
<td><code>title</code></td>
<td>string</td>
<td>required</td>
</tr>
<tr>
<td><code>description</code></td>
<td>string</td>
<td>required</td>
</tr>
<tr>
<td><code>address.lat</code></td>
<td>float</td>
<td>optional</td>
</tr>
<tr>
<td><code>address.lng</code></td>
<td>float</td>
<td>optional</td>
</tr>
<tr>
<td><code>address.street</code></td>
<td>string</td>
<td>required</td>
<td>max 255</td>
</tr>
<tr>
<td><code>address.street_number</code></td>
<td>string</td>
<td>required</td>
<td>max 50</td>
</tr>
<tr>
<td><code>address.zip</code></td>
<td>string</td>
<td>required</td>
<td>max 50</td>
</tr>
<tr>
<td><code>address.city</code></td>
<td>string</td>
<td>required</td>
<td>max 150</td>
</tr>
</tbody>
</table>
<!-- END_d1a366aa47ee59c96780bfe89ca95bdd -->
<!-- START_62d96e2c27434ddb7c604817f783bed8 -->
<h2>Get a specific project</h2>
<p><br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small></p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X GET \
    -G "https://weyounite.dev/api/projects/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "https://weyounite.dev/api/projects/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response =&gt; response.json())
    .then(json =&gt; console.log(json));</code></pre>
<pre><code class="language-php">
$client = new \GuzzleHttp\Client();
$response = $client-&gt;get(
    'https://weyounite.dev/api/projects/1',
    [
        'headers' =&gt; [
            'Content-Type' =&gt; 'application/json',
            'Accept' =&gt; 'application/json',
            'Authorization' =&gt; 'Bearer {token}',
        ],
    ]
);
$body = $response-&gt;getBody();
print_r(json_decode((string) $body));</code></pre>
<blockquote>
<p>Example response (401):</p>
</blockquote>
<pre><code class="language-json">{
    "message": "Unauthenticated."
}</code></pre>
<h3>HTTP Request</h3>
<p><code>GET api/projects/{project}</code></p>
<h4>URL Parameters</h4>
<table>
<thead>
<tr>
<th>Parameter</th>
<th>Status</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr>
<td><code>id</code></td>
<td>required</td>
<td>ID of the resource you want to show</td>
</tr>
</tbody>
</table>
<!-- END_62d96e2c27434ddb7c604817f783bed8 -->
<!-- START_1ca24c8d80ca22395fc07995d7929c34 -->
<h2>Update a project</h2>
<p><br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small></p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X PUT \
    "https://weyounite.dev/api/projects/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}" \
    -d '{"title":"laudantium","description":"quisquam","address":{"lat":50.0989809,"lng":52.98786799,"street":"Hauptstrasse","street_number":"45a","zip":"08678","city":"Buxtehude"}}'
</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "https://weyounite.dev/api/projects/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

let body = {
    "title": "laudantium",
    "description": "quisquam",
    "address": {
        "lat": 50.0989809,
        "lng": 52.98786799,
        "street": "Hauptstrasse",
        "street_number": "45a",
        "zip": "08678",
        "city": "Buxtehude"
    }
}

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response =&gt; response.json())
    .then(json =&gt; console.log(json));</code></pre>
<pre><code class="language-php">
$client = new \GuzzleHttp\Client();
$response = $client-&gt;put(
    'https://weyounite.dev/api/projects/1',
    [
        'headers' =&gt; [
            'Content-Type' =&gt; 'application/json',
            'Accept' =&gt; 'application/json',
            'Authorization' =&gt; 'Bearer {token}',
        ],
        'json' =&gt; [
            'title' =&gt; 'laudantium',
            'description' =&gt; 'quisquam',
            'address' =&gt; [
                'lat' =&gt; 50.0989809,
                'lng' =&gt; 52.98786799,
                'street' =&gt; 'Hauptstrasse',
                'street_number' =&gt; '45a',
                'zip' =&gt; '08678',
                'city' =&gt; 'Buxtehude',
            ],
        ],
    ]
);
$body = $response-&gt;getBody();
print_r(json_decode((string) $body));</code></pre>
<h3>HTTP Request</h3>
<p><code>PUT api/projects/{project}</code></p>
<p><code>PATCH api/projects/{project}</code></p>
<h4>URL Parameters</h4>
<table>
<thead>
<tr>
<th>Parameter</th>
<th>Status</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr>
<td><code>id</code></td>
<td>required</td>
<td>ID of the resource you want to update</td>
</tr>
</tbody>
</table>
<h4>Body Parameters</h4>
<table>
<thead>
<tr>
<th>Parameter</th>
<th>Type</th>
<th>Status</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr>
<td><code>title</code></td>
<td>string</td>
<td>required</td>
</tr>
<tr>
<td><code>description</code></td>
<td>string</td>
<td>required</td>
</tr>
<tr>
<td><code>address.lat</code></td>
<td>float</td>
<td>optional</td>
</tr>
<tr>
<td><code>address.lng</code></td>
<td>float</td>
<td>optional</td>
</tr>
<tr>
<td><code>address.street</code></td>
<td>string</td>
<td>required</td>
<td>max 255</td>
</tr>
<tr>
<td><code>address.street_number</code></td>
<td>string</td>
<td>required</td>
<td>max 50</td>
</tr>
<tr>
<td><code>address.zip</code></td>
<td>string</td>
<td>required</td>
<td>max 50</td>
</tr>
<tr>
<td><code>address.city</code></td>
<td>string</td>
<td>required</td>
<td>max 150</td>
</tr>
</tbody>
</table>
<!-- END_1ca24c8d80ca22395fc07995d7929c34 -->
<!-- START_70c859bdcb978e6cdba659235c2083d3 -->
<h2>Delete a project</h2>
<p><br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small></p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X DELETE \
    "https://weyounite.dev/api/projects/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "https://weyounite.dev/api/projects/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response =&gt; response.json())
    .then(json =&gt; console.log(json));</code></pre>
<pre><code class="language-php">
$client = new \GuzzleHttp\Client();
$response = $client-&gt;delete(
    'https://weyounite.dev/api/projects/1',
    [
        'headers' =&gt; [
            'Content-Type' =&gt; 'application/json',
            'Accept' =&gt; 'application/json',
            'Authorization' =&gt; 'Bearer {token}',
        ],
    ]
);
$body = $response-&gt;getBody();
print_r(json_decode((string) $body));</code></pre>
<h3>HTTP Request</h3>
<p><code>DELETE api/projects/{project}</code></p>
<h4>URL Parameters</h4>
<table>
<thead>
<tr>
<th>Parameter</th>
<th>Status</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr>
<td><code>id</code></td>
<td>required</td>
<td>ID of the resource you want to delete</td>
</tr>
</tbody>
</table>
<!-- END_70c859bdcb978e6cdba659235c2083d3 -->
<!-- START_ad8364edcb9cda916e656afaac7ca0cb -->
<h2>Upload a file to a project</h2>
<p><br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small></p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X POST \
    "https://weyounite.dev/api/projects/upload/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}" \
    -d '{"file":"ea"}'
</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "https://weyounite.dev/api/projects/upload/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

let body = {
    "file": "ea"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response =&gt; response.json())
    .then(json =&gt; console.log(json));</code></pre>
<pre><code class="language-php">
$client = new \GuzzleHttp\Client();
$response = $client-&gt;post(
    'https://weyounite.dev/api/projects/upload/1',
    [
        'headers' =&gt; [
            'Content-Type' =&gt; 'application/json',
            'Accept' =&gt; 'application/json',
            'Authorization' =&gt; 'Bearer {token}',
        ],
        'json' =&gt; [
            'file' =&gt; 'ea',
        ],
    ]
);
$body = $response-&gt;getBody();
print_r(json_decode((string) $body));</code></pre>
<h3>HTTP Request</h3>
<p><code>POST api/projects/upload/{project}</code></p>
<h4>URL Parameters</h4>
<table>
<thead>
<tr>
<th>Parameter</th>
<th>Status</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr>
<td><code>id</code></td>
<td>optional</td>
<td>int required ID of the resource where you want to upload a file</td>
</tr>
</tbody>
</table>
<h4>Body Parameters</h4>
<table>
<thead>
<tr>
<th>Parameter</th>
<th>Type</th>
<th>Status</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr>
<td><code>file</code></td>
<td>required</td>
<td>optional</td>
<td>A file you want to upload</td>
</tr>
</tbody>
</table>
<!-- END_ad8364edcb9cda916e656afaac7ca0cb -->
<h1>SupportedProjects</h1>
<!-- START_054c0b1fe654b7398149a5c360b7d7cd -->
<h2>Get the projects supported</h2>
<p><br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small></p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X GET \
    -G "https://weyounite.dev/api/projects_supported" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "https://weyounite.dev/api/projects_supported"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response =&gt; response.json())
    .then(json =&gt; console.log(json));</code></pre>
<pre><code class="language-php">
$client = new \GuzzleHttp\Client();
$response = $client-&gt;get(
    'https://weyounite.dev/api/projects_supported',
    [
        'headers' =&gt; [
            'Content-Type' =&gt; 'application/json',
            'Accept' =&gt; 'application/json',
            'Authorization' =&gt; 'Bearer {token}',
        ],
    ]
);
$body = $response-&gt;getBody();
print_r(json_decode((string) $body));</code></pre>
<blockquote>
<p>Example response (401):</p>
</blockquote>
<pre><code class="language-json">{
    "message": "Unauthenticated."
}</code></pre>
<h3>HTTP Request</h3>
<p><code>GET api/projects_supported</code></p>
<!-- END_054c0b1fe654b7398149a5c360b7d7cd -->
<!-- START_a4a57018eeccc000a5330dd19bbeb53f -->
<h2>Support a project</h2>
<p><br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small></p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X POST \
    "https://weyounite.dev/api/projects_supported" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}" \
    -d '{"user_id":6,"project_id":20}'
</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "https://weyounite.dev/api/projects_supported"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

let body = {
    "user_id": 6,
    "project_id": 20
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response =&gt; response.json())
    .then(json =&gt; console.log(json));</code></pre>
<pre><code class="language-php">
$client = new \GuzzleHttp\Client();
$response = $client-&gt;post(
    'https://weyounite.dev/api/projects_supported',
    [
        'headers' =&gt; [
            'Content-Type' =&gt; 'application/json',
            'Accept' =&gt; 'application/json',
            'Authorization' =&gt; 'Bearer {token}',
        ],
        'json' =&gt; [
            'user_id' =&gt; 6,
            'project_id' =&gt; 20,
        ],
    ]
);
$body = $response-&gt;getBody();
print_r(json_decode((string) $body));</code></pre>
<h3>HTTP Request</h3>
<p><code>POST api/projects_supported</code></p>
<h4>Body Parameters</h4>
<table>
<thead>
<tr>
<th>Parameter</th>
<th>Type</th>
<th>Status</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr>
<td><code>user_id</code></td>
<td>integer</td>
<td>required</td>
<td>ID of the supporter</td>
</tr>
<tr>
<td><code>project_id</code></td>
<td>integer</td>
<td>required</td>
<td>ID of the project</td>
</tr>
</tbody>
</table>
<!-- END_a4a57018eeccc000a5330dd19bbeb53f -->
<!-- START_9e45d803b779ea36a0066014e8bef978 -->
<h2>Show a specific support</h2>
<p><br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small></p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X GET \
    -G "https://weyounite.dev/api/projects_supported/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "https://weyounite.dev/api/projects_supported/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response =&gt; response.json())
    .then(json =&gt; console.log(json));</code></pre>
<pre><code class="language-php">
$client = new \GuzzleHttp\Client();
$response = $client-&gt;get(
    'https://weyounite.dev/api/projects_supported/1',
    [
        'headers' =&gt; [
            'Content-Type' =&gt; 'application/json',
            'Accept' =&gt; 'application/json',
            'Authorization' =&gt; 'Bearer {token}',
        ],
    ]
);
$body = $response-&gt;getBody();
print_r(json_decode((string) $body));</code></pre>
<blockquote>
<p>Example response (401):</p>
</blockquote>
<pre><code class="language-json">{
    "message": "Unauthenticated."
}</code></pre>
<h3>HTTP Request</h3>
<p><code>GET api/projects_supported/{projects_supported}</code></p>
<h4>URL Parameters</h4>
<table>
<thead>
<tr>
<th>Parameter</th>
<th>Status</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr>
<td><code>id</code></td>
<td>required</td>
<td>ID of the resource you want to fetch</td>
</tr>
</tbody>
</table>
<!-- END_9e45d803b779ea36a0066014e8bef978 -->
<!-- START_c9fd14402856842e5b972b724bd1cca2 -->
<h2>Update a support</h2>
<p><br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small></p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X PUT \
    "https://weyounite.dev/api/projects_supported/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}" \
    -d '{"user_id":16,"project_id":20}'
</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "https://weyounite.dev/api/projects_supported/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

let body = {
    "user_id": 16,
    "project_id": 20
}

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response =&gt; response.json())
    .then(json =&gt; console.log(json));</code></pre>
<pre><code class="language-php">
$client = new \GuzzleHttp\Client();
$response = $client-&gt;put(
    'https://weyounite.dev/api/projects_supported/1',
    [
        'headers' =&gt; [
            'Content-Type' =&gt; 'application/json',
            'Accept' =&gt; 'application/json',
            'Authorization' =&gt; 'Bearer {token}',
        ],
        'json' =&gt; [
            'user_id' =&gt; 16,
            'project_id' =&gt; 20,
        ],
    ]
);
$body = $response-&gt;getBody();
print_r(json_decode((string) $body));</code></pre>
<h3>HTTP Request</h3>
<p><code>PUT api/projects_supported/{projects_supported}</code></p>
<p><code>PATCH api/projects_supported/{projects_supported}</code></p>
<h4>URL Parameters</h4>
<table>
<thead>
<tr>
<th>Parameter</th>
<th>Status</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr>
<td><code>id</code></td>
<td>required</td>
<td>ID of the resource you want to update</td>
</tr>
</tbody>
</table>
<h4>Body Parameters</h4>
<table>
<thead>
<tr>
<th>Parameter</th>
<th>Type</th>
<th>Status</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr>
<td><code>user_id</code></td>
<td>integer</td>
<td>required</td>
<td>ID of the supporter</td>
</tr>
<tr>
<td><code>project_id</code></td>
<td>integer</td>
<td>required</td>
<td>ID of the project</td>
</tr>
</tbody>
</table>
<!-- END_c9fd14402856842e5b972b724bd1cca2 -->
<!-- START_ab803ebb1390863458d42b2ba66d769f -->
<h2>Delete a support</h2>
<p><br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small></p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X DELETE \
    "https://weyounite.dev/api/projects_supported/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "https://weyounite.dev/api/projects_supported/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response =&gt; response.json())
    .then(json =&gt; console.log(json));</code></pre>
<pre><code class="language-php">
$client = new \GuzzleHttp\Client();
$response = $client-&gt;delete(
    'https://weyounite.dev/api/projects_supported/1',
    [
        'headers' =&gt; [
            'Content-Type' =&gt; 'application/json',
            'Accept' =&gt; 'application/json',
            'Authorization' =&gt; 'Bearer {token}',
        ],
    ]
);
$body = $response-&gt;getBody();
print_r(json_decode((string) $body));</code></pre>
<h3>HTTP Request</h3>
<p><code>DELETE api/projects_supported/{projects_supported}</code></p>
<h4>URL Parameters</h4>
<table>
<thead>
<tr>
<th>Parameter</th>
<th>Status</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr>
<td><code>id</code></td>
<td>optional</td>
<td>int required ID of the resource you want to delete</td>
</tr>
</tbody>
</table>
<!-- END_ab803ebb1390863458d42b2ba66d769f -->
<h1>UserManagement</h1>
<!-- START_fc1e4f6a697e3c48257de845299b71d5 -->
<h2>Get User</h2>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X GET \
    -G "https://weyounite.dev/api/users" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "https://weyounite.dev/api/users"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response =&gt; response.json())
    .then(json =&gt; console.log(json));</code></pre>
<pre><code class="language-php">
$client = new \GuzzleHttp\Client();
$response = $client-&gt;get(
    'https://weyounite.dev/api/users',
    [
        'headers' =&gt; [
            'Content-Type' =&gt; 'application/json',
            'Accept' =&gt; 'application/json',
            'Authorization' =&gt; 'Bearer {token}',
        ],
    ]
);
$body = $response-&gt;getBody();
print_r(json_decode((string) $body));</code></pre>
<blockquote>
<p>Example response (401):</p>
</blockquote>
<pre><code class="language-json">{
    "message": "Unauthenticated."
}</code></pre>
<h3>HTTP Request</h3>
<p><code>GET api/users</code></p>
<!-- END_fc1e4f6a697e3c48257de845299b71d5 -->
<!-- START_8653614346cb0e3d444d164579a0a0a2 -->
<h2>Get a specific user</h2>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X GET \
    -G "https://weyounite.dev/api/users/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "https://weyounite.dev/api/users/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response =&gt; response.json())
    .then(json =&gt; console.log(json));</code></pre>
<pre><code class="language-php">
$client = new \GuzzleHttp\Client();
$response = $client-&gt;get(
    'https://weyounite.dev/api/users/1',
    [
        'headers' =&gt; [
            'Content-Type' =&gt; 'application/json',
            'Accept' =&gt; 'application/json',
            'Authorization' =&gt; 'Bearer {token}',
        ],
    ]
);
$body = $response-&gt;getBody();
print_r(json_decode((string) $body));</code></pre>
<blockquote>
<p>Example response (401):</p>
</blockquote>
<pre><code class="language-json">{
    "message": "Unauthenticated."
}</code></pre>
<h3>HTTP Request</h3>
<p><code>GET api/users/{user}</code></p>
<h4>URL Parameters</h4>
<table>
<thead>
<tr>
<th>Parameter</th>
<th>Status</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr>
<td><code>id</code></td>
<td>optional</td>
<td>int required ID of the resource you want to fetch</td>
</tr>
</tbody>
</table>
<!-- END_8653614346cb0e3d444d164579a0a0a2 -->
<!-- START_d2db7a9fe3abd141d5adbc367a88e969 -->
<h2>Delete an user</h2>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X DELETE \
    "https://weyounite.dev/api/users/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "https://weyounite.dev/api/users/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response =&gt; response.json())
    .then(json =&gt; console.log(json));</code></pre>
<pre><code class="language-php">
$client = new \GuzzleHttp\Client();
$response = $client-&gt;delete(
    'https://weyounite.dev/api/users/1',
    [
        'headers' =&gt; [
            'Content-Type' =&gt; 'application/json',
            'Accept' =&gt; 'application/json',
            'Authorization' =&gt; 'Bearer {token}',
        ],
    ]
);
$body = $response-&gt;getBody();
print_r(json_decode((string) $body));</code></pre>
<h3>HTTP Request</h3>
<p><code>DELETE api/users/{user}</code></p>
<h4>URL Parameters</h4>
<table>
<thead>
<tr>
<th>Parameter</th>
<th>Status</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr>
<td><code>id</code></td>
<td>required</td>
<td>ID of the resource you want to delete</td>
</tr>
</tbody>
</table>
<!-- END_d2db7a9fe3abd141d5adbc367a88e969 -->
<!-- START_594aa1261378686359a7b4f03517d57b -->
<h2>Upload file for the user</h2>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X POST \
    "https://weyounite.dev/api/users/upload/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}" \
    -d '{"file":"inventore"}'
</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "https://weyounite.dev/api/users/upload/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

let body = {
    "file": "inventore"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response =&gt; response.json())
    .then(json =&gt; console.log(json));</code></pre>
<pre><code class="language-php">
$client = new \GuzzleHttp\Client();
$response = $client-&gt;post(
    'https://weyounite.dev/api/users/upload/1',
    [
        'headers' =&gt; [
            'Content-Type' =&gt; 'application/json',
            'Accept' =&gt; 'application/json',
            'Authorization' =&gt; 'Bearer {token}',
        ],
        'json' =&gt; [
            'file' =&gt; 'inventore',
        ],
    ]
);
$body = $response-&gt;getBody();
print_r(json_decode((string) $body));</code></pre>
<h3>HTTP Request</h3>
<p><code>POST api/users/upload/{user}</code></p>
<h4>URL Parameters</h4>
<table>
<thead>
<tr>
<th>Parameter</th>
<th>Status</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr>
<td><code>id</code></td>
<td>required</td>
<td>ID of the resource where you want to upload a file</td>
</tr>
</tbody>
</table>
<h4>Body Parameters</h4>
<table>
<thead>
<tr>
<th>Parameter</th>
<th>Type</th>
<th>Status</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr>
<td><code>file</code></td>
<td>required</td>
<td>optional</td>
<td>A file you want to upload</td>
</tr>
</tbody>
</table>
<!-- END_594aa1261378686359a7b4f03517d57b -->
      </div>
      <div class="dark-box">
                        <div class="lang-selector">
                                    <a href="#" data-language-name="bash">bash</a>
                                    <a href="#" data-language-name="javascript">javascript</a>
                                    <a href="#" data-language-name="php">php</a>
                              </div>
                </div>
    </div>
  </body>
</html>