@extends('layouts.app')

@section('content')
<div id="vue">
    <project-carousel></project-carousel>
    <register-buttons></register-buttons>
    <projects-near></projects-near>
</div>
@endsection
