<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class CreateHelperTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testExample()
    {
        Sanctum::actingAs($this->getTestUser());

        $newHelper = [
            //PersonAttributes
            'person' => [
                'contact' => 'contact@me.com',
                'has_privacy_settings_accepted' => 0,
                'has_agb_accepted'=> 1,
                'description' => 'some description',
                'first_name' => 'vorname',
                'last_name' => 'nachname',
            ],
            //AddressAttributes
            'address' => [
                'street' => 'strasse',
                'street_number' => '12',
                'zip' => '08539',
                'city' => 'city'
            ]

        ];

        $response = $this->postJson('api/helper/' . $this->getTestUser()->id, $newHelper);

        $response->assertStatus(200);

        $response->assertJsonMissing(['success' => false]);
        $response->assertJsonFragment(['street' => 'strasse']);
    }
}
