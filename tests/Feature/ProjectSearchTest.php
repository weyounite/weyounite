<?php

namespace Tests\Feature;

use App\Address;
use App\Project;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ProjectSearchTest extends TestCase
{
    /* this is not possible at the moment to can be tested cause the sql query cannot be tested in sqlite -
    * andswitching to mysql at testing is a very slow result
    public function testSearchByRadius()
    {
        $project = factory(Project::class)->create();

        $project->address()->save(factory(Address::class)->make([
            'lat' => 50.4630009, 'lng' => 12.068337
        ]));

        $response = $this->postJson('api/projects/search',[
            'lat' => 50.500000,
            'lng' => 12.069000,
            'radius' => 10
        ]);
        $response->assertStatus(200);
    }
    */
}
