<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class CreateCompanyTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testCreateCompany()
    {
        Sanctum::actingAs($this->getTestUser());

        $newCompany = [
            'name' => 'myCompany',
            'established_at' =>'20-12-1998',
            'staff_count' => '5',
            'description' => 'ne beschreibung der firma',
            'challenges' => 'some challenges',
            'strengths' => 'meine staerken',
            //PersonAttributes
            'person' => [
                'contact' => 'contact@me.com',
                'has_privacy_settings_accepted' => 0,
                'has_agb_accepted'=> 1,
                'description' => 'ich bin business owner',
                'first_name' => 'vorname',
                'last_name' => 'nachname',
            ],
            //AddressAttributes
            'address' => [
                'street' => 'strasse',
                'street_number' => '12',
                'zip' => '08539',
                'city' => 'city'
            ]

        ];

        $response = $this->postJson('api/companies/' . $this->getTestUser()->id, $newCompany);

        $response->assertStatus(200);

        $response->assertJsonMissing(['success' => false]);
        $response->assertJsonFragment(['name' => 'myCompany']);
    }
}
