<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class SanctumLoginTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testLognRequired()
    {
        $response = $this->get('/api/users');

        $response->assertRedirect();
    }

    public function testGetAccessWhenLoggedIn()
    {
        Sanctum::actingAs($this->getTestUser());

        $response = $this->get('/api/users');

        $response->assertStatus(200);
    }

    public function testGetTokenByAuth()
    {
        $response = $this->postJson('/api/auth/token', [
            'email' => 'dk.projects.manager@gmail.com',
            'password' => '12345',
            'device_name' => 'apple'
        ]);

        $response->assertStatus(200);

        $this->assertNotEmpty($response->content());
    }
}
