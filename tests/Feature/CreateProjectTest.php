<?php

namespace Tests\Feature;

use App\Project;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class CreateProjectTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testCreateProject()
    {
        Sanctum::actingAs($this->getTestUser());

        $newProject = [
            'title' => 'mein project',
            'description' => 'ne beschreibung von dem was ich brauche',
            //AddressAttributes
            'address' => [
                'street' => 'strasse',
                'street_number' => '12',
                'zip' => '08539',
                'city' => 'city'
            ]

        ];

        $response = $this->postJson('api/projects/', $newProject);

        $response->assertStatus(201);

        $response->assertJsonMissing(['success' => false]);
        $response->assertJsonFragment(['title' => 'mein project', 'street' => 'strasse']);
    }

    public function testUpdateProject()
    {
        Sanctum::actingAs($this->getTestUser());

        $project = Project::first();

        $update = [
            'title' => 'mein project teil 2',
            'description' => 'ne beschreibung von dem was ich brauche',
            //AddressAttributes
            'address' => [
                'street' => 'strasse update',
                'street_number' => '12',
                'zip' => '08539',
                'city' => 'city'
            ]

        ];

        $response = $this->putJson('api/projects/' . $project->id, $update);

        $response->assertStatus(200);

        $response->assertJsonMissing(['success' => false]);
        $response->assertJsonFragment(['title' => 'mein project teil 2', 'street' => 'strasse update', 'id' => $project->id]);
    }
}
