# search

## POST /api/projects/search

### body 

```json
{
	"lat" : "50.93318300",
	"lng" : "6.94668500",
	"perimeter": "50"
}
```

### response

```json
{
  "data": [
    {
      "id": 1,
      "supporters": [
        {
          "id": 4,
          "name": "Markus",
          "email": "markus@email.de",
          "lat": null,
          "lng": null,
          "contact": "markus@email.de"
        }
      ],
      "owner": {
        "id": 2,
        "name": "Melanie",
        "email": "fit-strong@email.de",
        "lat": "50.93318500",
        "lng": "6.94668500",
        "contact": "fit-strong@email.de"
      },
      "title": "Fit & Strong",
      "description": "Mein Studio ist mein Lebenstraum – jeder Kurs ist extra aufgesetzt, ich arbeite jede Woche dran, den Teilnhmern neue Ideen, Inhalt und Übungen zu geben und den Spaß in den Vordergrund zu stellen, den Teamspirit beim gemeinsamen trainieren. Kleine Kurse, immer im Kontakt und jede Woche etwas besser – das will ich jedem bieten können! Mein Studio habe ich schon einen Tag vor der Anordnung geschlossen – dass meine Teilnehmer gesund bleiben war mir extrem wichtig. Das nimmt nicht nur dem Studio die Einnahmen, es nimmt auch meinen Teilnehmern die Chance, gemeinsam an sich selbst zu arbeiten. Beides zusammen ist für mich die größte Herausfordeurng. Natürlich helfen mir Spenden, das Studio über die Zeit zu retten! Aber noch wichtiger für mich und alle, die mit mir trainieren: Helft mir, die Kurse in jede Wohnung zu bringen! Egal ob Hilfe beim Filmen, bei der Bereitstellung der Inhalte oder beim Aufbau des online Service. Mit eurer Hilfe bleibt Fit&Strong eine starke Gemeinschaft!",
      "distance": 0.0002325430385957901,
      "contact": "fit-strong@email.de",
      "lat": "50.93318500",
      "lng": "6.94668500",
      "files": [
        "http:\/\/localhost\/storage\/7\/41089685_1928714300522312_1189528816540712960_o.jpg",
        "http:\/\/localhost\/storage\/8\/41089685_1928714300522312_1189528816540712960_o.jpg",
        "http:\/\/localhost\/storage\/9\/41089685_1928714300522312_1189528816540712960_o.jpg",
        "http:\/\/localhost\/storage\/10\/41089685_1928714300522312_1189528816540712960_o.jpg"
      ]
    },
    {
      "id": 3,
      "supporters": [],
      "owner": {
        "id": 3,
        "name": "Carina & Massimo",
        "email": "santos@email.de",
        "lat": "50.92565800",
        "lng": "6.95598000",
        "contact": "santos@email.de"
      },
      "title": "Santos",
      "description": "Carina und ich teilen die Leidenschaft für guten Wein, noch besseres Essen und das Gefühl abends im Innenhof eines italienischen Restaurants den Tag ausklingen zu lassen. Und genau diese Leidenschaft soll jeder Gast bei uns spüren. So viele frische leckere Zutaten wie möglich, zwei Köche mit Liebe für den perfekten Geschmack und italienische Musik. Und Gäste, die immer wieder gerne mit uns in das Gefühl des letzten Urlaubs tauchen. Wir können aktuell zwar weiter Essen an unsere Kunden liefern – aber das Gefühl von Bella Italia fehlt. Für uns und die Miete können wir damit sorgen, aber wir haben einfach nicht die Arbeit für unser gesamtes Team. Und jeder einzelne und seine Familie gehören zu unserer Famiglia. Ihr könnt uns unterstützen, das Bella Italia Gefühl nach dieser Zeit wieder zu euch zu bringen! Mit jedem Gutschein und jeder Spende kommen wir diesem Ziel einen Schritt näher. Und wenn ihr Ideen habt, wie wir in dieser Zeit online oder Offline uns verbessern können – jeder ist herzlich eingeladen, Teil der Famiglia Santos zu werden!",
      "distance": 1.0604218357966901,
      "contact": "santos@email.de",
      "lat": "50.92565800",
      "lng": "6.95598000",
      "files": []
    },
    {
      "id": 2,
      "supporters": [
        {
          "id": 5,
          "name": "Annabel",
          "email": "corinna@email.de",
          "lat": null,
          "lng": null,
          "contact": "corinna@email.de"
        }
      ],
      "owner": {
        "id": 1,
        "name": "Paola",
        "email": "paola_hair@email.de",
        "lat": "50.93556800",
        "lng": "6.92018900",
        "contact": "+49\/17610010010"
      },
      "title": "Hair by Paola",
      "description": "Wir sind ein kleines Team von Friseuren mit einer gemeinsamen Leidenschaft – das Beste aus den Haaren jedes unserer Kunden zu holen! Gemeinsam kreieren wir nicht nur tolle Haarmode sondern vor allem auch ein Ambiente, in dem sich unsere Kunden wohlfühlen und entspannt die Zeit bei uns genießen können. Erst wurden die Kunden weniger, dann mussten wir unseren Salon schließen. Die aktuelle Situation führt dazu, dass wir unserer Leidenschaft nicht mehr nachgehen können. Di Rechnungen aber bleiben. Ein solches Team kann ich nicht einfach ersetzen – deshalb setzen wir auf die Hilfe jedes Einzelnen, um das, was wir gemeinsam aufgebaut haben, zu retten. Helft uns – durch den Kauf eines Gutscheins, eine Spende oder indem ihr andere dazu ermutigt, genau das zu tun! Wir wollen nicht reich werden, sondern das was wir aufgebaut haben durch diese Zeit retten! Da wir in dieser Zeit auch einiges reparieren, ist jede helfende Hand auch gerne willkommen!",
      "distance": 1.8755818299193356,
      "contact": "+49\/17610010010",
      "lat": "50.93556800",
      "lng": "6.92018900",
      "files": []
    }
  ]
}
```

# auth 

## POST /api/auth/token

### body
```json
{
  "email": "",
  "password": "",
  "device_name": ""
}
```

wii return a token you need to authenticate as a Bearer one.

### response

# projects

## GET /api/projects

### response
```json
{
  "current_page": 1,
  "data": [
    {
      "id": 1,
      "title": "Fit & Strong",
      "description": "Mein Studio ist mein Lebenstraum – jeder Kurs ist extra aufgesetzt, ich arbeite jede Woche dran, den Teilnhmern neue Ideen, Inhalt und Übungen zu geben und den Spaß in den Vordergrund zu stellen, den Teamspirit beim gemeinsamen trainieren. Kleine Kurse, immer im Kontakt und jede Woche etwas besser – das will ich jedem bieten können! Mein Studio habe ich schon einen Tag vor der Anordnung geschlossen – dass meine Teilnehmer gesund bleiben war mir extrem wichtig. Das nimmt nicht nur dem Studio die Einnahmen, es nimmt auch meinen Teilnehmern die Chance, gemeinsam an sich selbst zu arbeiten. Beides zusammen ist für mich die größte Herausfordeurng. Natürlich helfen mir Spenden, das Studio über die Zeit zu retten! Aber noch wichtiger für mich und alle, die mit mir trainieren: Helft mir, die Kurse in jede Wohnung zu bringen! Egal ob Hilfe beim Filmen, bei der Bereitstellung der Inhalte oder beim Aufbau des online Service. Mit eurer Hilfe bleibt Fit&Strong eine starke Gemeinschaft!",
      "contact": "fit-strong@email.de",
      "lat": "50.93318500",
      "lng": "6.94668500",
      "created_at": null,
      "updated_at": null,
      "deleted_at": null,
      "owner": {
        "id": 2,
        "name": "Melanie",
        "email": "fit-strong@email.de",
        "email_verified_at": "2020-03-22T19:08:35.000000Z",
        "contact": "fit-strong@email.de",
        "lat": "50.93318500",
        "lng": "6.94668500",
        "created_at": null,
        "updated_at": null,
        "deleted_at": null,
        "laravel_through_key": 1
      },
      "supporters": [
        {
          "id": 4,
          "name": "Markus",
          "email": "markus@email.de",
          "email_verified_at": "2020-03-22T19:08:35.000000Z",
          "contact": "markus@email.de",
          "lat": null,
          "lng": null,
          "created_at": null,
          "updated_at": null,
          "deleted_at": null,
          "laravel_through_key": 1
        }
      ]
    },
    {
      "id": 2,
      "title": "Hair by Paola",
      "description": "Wir sind ein kleines Team von Friseuren mit einer gemeinsamen Leidenschaft – das Beste aus den Haaren jedes unserer Kunden zu holen! Gemeinsam kreieren wir nicht nur tolle Haarmode sondern vor allem auch ein Ambiente, in dem sich unsere Kunden wohlfühlen und entspannt die Zeit bei uns genießen können. Erst wurden die Kunden weniger, dann mussten wir unseren Salon schließen. Die aktuelle Situation führt dazu, dass wir unserer Leidenschaft nicht mehr nachgehen können. Di Rechnungen aber bleiben. Ein solches Team kann ich nicht einfach ersetzen – deshalb setzen wir auf die Hilfe jedes Einzelnen, um das, was wir gemeinsam aufgebaut haben, zu retten. Helft uns – durch den Kauf eines Gutscheins, eine Spende oder indem ihr andere dazu ermutigt, genau das zu tun! Wir wollen nicht reich werden, sondern das was wir aufgebaut haben durch diese Zeit retten! Da wir in dieser Zeit auch einiges reparieren, ist jede helfende Hand auch gerne willkommen!",
      "contact": "+49\/17610010010",
      "lat": "50.93556800",
      "lng": "6.92018900",
      "created_at": null,
      "updated_at": null,
      "deleted_at": null,
      "owner": {
        "id": 1,
        "name": "Paola",
        "email": "paola_hair@email.de",
        "email_verified_at": "2020-03-22T19:08:35.000000Z",
        "contact": "+49\/17610010010",
        "lat": "50.93556800",
        "lng": "6.92018900",
        "created_at": null,
        "updated_at": null,
        "deleted_at": null,
        "laravel_through_key": 2
      },
      "supporters": [
        {
          "id": 5,
          "name": "Annabel",
          "email": "corinna@email.de",
          "email_verified_at": "2020-03-22T19:08:35.000000Z",
          "contact": "corinna@email.de",
          "lat": null,
          "lng": null,
          "created_at": null,
          "updated_at": null,
          "deleted_at": null,
          "laravel_through_key": 2
        }
      ]
    },
    {
      "id": 3,
      "title": "Santos",
      "description": "Carina und ich teilen die Leidenschaft für guten Wein, noch besseres Essen und das Gefühl abends im Innenhof eines italienischen Restaurants den Tag ausklingen zu lassen. Und genau diese Leidenschaft soll jeder Gast bei uns spüren. So viele frische leckere Zutaten wie möglich, zwei Köche mit Liebe für den perfekten Geschmack und italienische Musik. Und Gäste, die immer wieder gerne mit uns in das Gefühl des letzten Urlaubs tauchen. Wir können aktuell zwar weiter Essen an unsere Kunden liefern – aber das Gefühl von Bella Italia fehlt. Für uns und die Miete können wir damit sorgen, aber wir haben einfach nicht die Arbeit für unser gesamtes Team. Und jeder einzelne und seine Familie gehören zu unserer Famiglia. Ihr könnt uns unterstützen, das Bella Italia Gefühl nach dieser Zeit wieder zu euch zu bringen! Mit jedem Gutschein und jeder Spende kommen wir diesem Ziel einen Schritt näher. Und wenn ihr Ideen habt, wie wir in dieser Zeit online oder Offline uns verbessern können – jeder ist herzlich eingeladen, Teil der Famiglia Santos zu werden!",
      "contact": "santos@email.de",
      "lat": "50.92565800",
      "lng": "6.95598000",
      "created_at": null,
      "updated_at": null,
      "deleted_at": null,
      "owner": {
        "id": 3,
        "name": "Carina & Massimo",
        "email": "santos@email.de",
        "email_verified_at": "2020-03-22T19:08:35.000000Z",
        "contact": "santos@email.de",
        "lat": "50.92565800",
        "lng": "6.95598000",
        "created_at": null,
        "updated_at": null,
        "deleted_at": null,
        "laravel_through_key": 3
      },
      "supporters": []
    }
  ],
  "first_page_url": "https:\/\/weyounite.dev\/api\/projects?page=1",
  "from": 1,
  "last_page": 1,
  "last_page_url": "https:\/\/weyounite.dev\/api\/projects?page=1",
  "next_page_url": null,
  "path": "https:\/\/weyounite.dev\/api\/projects",
  "per_page": 15,
  "prev_page_url": null,
  "to": 3,
  "total": 3
}
```

## POST /api/projects

### body

```json
{
	"title": "mein neues  project",
	"description" : "das ist meine beschreibung",
	"lat": "50.445080",
	"lng": "12.062670"
}
```

### response

```json
{
  "data": {
    "title": "mein neues  project",
    "description": "das ist meine beschreibung",
    "lat": "50.44508000",
    "lng": "12.06267000",
    "updated_at": "2020-03-23T18:28:12.000000Z",
    "created_at": "2020-03-23T18:28:12.000000Z",
    "id": 5
  }
}
```

## PUT /api/projects/{id}

```json
{
  "title": "mein project",
  "description": "das ist meine beschreibung 2"
}
```

### response

```json
{
  "data": {
    "id": 2,
    "title": "mein project",
    "description": "das ist meine beschreibung 2",
    "created_at": "2020-03-21T23:08:27.000000Z",
    "updated_at": "2020-03-21T23:11:11.000000Z",
    "deleted_at": null
  }
}
```

## DELETE /api/projects{id}

### response

```json
{
  "sucess": true
}
```

## POST /api/projects/upload/{1}

upload a file with attribute 'file'

### response

```json
{
  "sucess": true
}
```

# users

## GET /api/users

### response

```json
{
  "data": [
    {
      "id": 1,
      "name": "Dr. Lorna Hansen",
      "email": "kristofer76@example.com",
      "first_name": "Sabryna",
      "last_name": "Nicolas",
      "contact": null,
      "helper": {
        "address": {
          "id": 1,
          "addressable_id": 1,
          "addressable_type": "App\\Helper",
          "street": "Alexane Shore",
          "street_number": "Islands",
          "zip": "96713-3966",
          "city": "South Elwinberg",
          "lat": "55.66726600",
          "lng": "113.61202500",
          "created_at": "2020-03-30T15:27:31.000000Z",
          "updated_at": "2020-03-30T15:27:31.000000Z",
          "deleted_at": null
        }
      },
      "company": null,
      "projectsOwned": [
        {
          "id": 2,
          "title": "Hair by Paola",
          "description": "Wir sind ein kleines Team von Friseuren mit einer gemeinsamen Leidenschaft – das Beste aus den Haaren jedes unserer Kunden zu holen! Gemeinsam kreieren wir nicht nur tolle Haarmode sondern vor allem auch ein Ambiente, in dem sich unsere Kunden wohlfühlen und entspannt die Zeit bei uns genießen können. Erst wurden die Kunden weniger, dann mussten wir unseren Salon schließen. Die aktuelle Situation führt dazu, dass wir unserer Leidenschaft nicht mehr nachgehen können. Di Rechnungen aber bleiben. Ein solches Team kann ich nicht einfach ersetzen – deshalb setzen wir auf die Hilfe jedes Einzelnen, um das, was wir gemeinsam aufgebaut haben, zu retten. Helft uns – durch den Kauf eines Gutscheins, eine Spende oder indem ihr andere dazu ermutigt, genau das zu tun! Wir wollen nicht reich werden, sondern das was wir aufgebaut haben durch diese Zeit retten! Da wir in dieser Zeit auch einiges reparieren, ist jede helfende Hand auch gerne willkommen!",
          "contact": "+49\/17610010010",
          "lat": "50.93556800",
          "lng": "6.92018900",
          "files": []
        }
      ],
      "projectsSupported": [],
      "files": []
    },
    {
      "id": 2,
      "name": "Hiram Boehm",
      "email": "kamron24@example.com",
      "first_name": "Bradly",
      "last_name": "Leannon",
      "contact": null,
      "helper": {
        "address": {
          "id": 2,
          "addressable_id": 2,
          "addressable_type": "App\\Helper",
          "street": "Myriam Coves",
          "street_number": "Drives",
          "zip": "80897-8170",
          "city": "Moorehaven",
          "lat": "-70.10473800",
          "lng": "64.10523200",
          "created_at": "2020-03-30T15:27:31.000000Z",
          "updated_at": "2020-03-30T15:27:31.000000Z",
          "deleted_at": null
        }
      },
      "company": null,
      "projectsOwned": [
        {
          "id": 1,
          "title": "Fit & Strong",
          "description": "Mein Studio ist mein Lebenstraum – jeder Kurs ist extra aufgesetzt, ich arbeite jede Woche dran, den Teilnhmern neue Ideen, Inhalt und Übungen zu geben und den Spaß in den Vordergrund zu stellen, den Teamspirit beim gemeinsamen trainieren. Kleine Kurse, immer im Kontakt und jede Woche etwas besser – das will ich jedem bieten können! Mein Studio habe ich schon einen Tag vor der Anordnung geschlossen – dass meine Teilnehmer gesund bleiben war mir extrem wichtig. Das nimmt nicht nur dem Studio die Einnahmen, es nimmt auch meinen Teilnehmern die Chance, gemeinsam an sich selbst zu arbeiten. Beides zusammen ist für mich die größte Herausfordeurng. Natürlich helfen mir Spenden, das Studio über die Zeit zu retten! Aber noch wichtiger für mich und alle, die mit mir trainieren: Helft mir, die Kurse in jede Wohnung zu bringen! Egal ob Hilfe beim Filmen, bei der Bereitstellung der Inhalte oder beim Aufbau des online Service. Mit eurer Hilfe bleibt Fit&Strong eine starke Gemeinschaft!",
          "contact": "fit-strong@email.de",
          "lat": "50.93318500",
          "lng": "6.94668500",
          "files": []
        }
      ],
      "projectsSupported": [],
      "files": []
    },
    {
      "id": 3,
      "name": "Dr. Meggie Schultz",
      "email": "kihn.vernice@example.net",
      "first_name": "Tito",
      "last_name": "Brown",
      "contact": null,
      "helper": {
        "address": {
          "id": 3,
          "addressable_id": 3,
          "addressable_type": "App\\Helper",
          "street": "Gleichner Pass",
          "street_number": "Terrace",
          "zip": "37167",
          "city": "Kaelaborough",
          "lat": "89.63615600",
          "lng": "-134.55070200",
          "created_at": "2020-03-30T15:27:31.000000Z",
          "updated_at": "2020-03-30T15:27:31.000000Z",
          "deleted_at": null
        }
      },
      "company": null,
      "projectsOwned": [
        {
          "id": 3,
          "title": "Santos",
          "description": "Carina und ich teilen die Leidenschaft für guten Wein, noch besseres Essen und das Gefühl abends im Innenhof eines italienischen Restaurants den Tag ausklingen zu lassen. Und genau diese Leidenschaft soll jeder Gast bei uns spüren. So viele frische leckere Zutaten wie möglich, zwei Köche mit Liebe für den perfekten Geschmack und italienische Musik. Und Gäste, die immer wieder gerne mit uns in das Gefühl des letzten Urlaubs tauchen. Wir können aktuell zwar weiter Essen an unsere Kunden liefern – aber das Gefühl von Bella Italia fehlt. Für uns und die Miete können wir damit sorgen, aber wir haben einfach nicht die Arbeit für unser gesamtes Team. Und jeder einzelne und seine Familie gehören zu unserer Famiglia. Ihr könnt uns unterstützen, das Bella Italia Gefühl nach dieser Zeit wieder zu euch zu bringen! Mit jedem Gutschein und jeder Spende kommen wir diesem Ziel einen Schritt näher. Und wenn ihr Ideen habt, wie wir in dieser Zeit online oder Offline uns verbessern können – jeder ist herzlich eingeladen, Teil der Famiglia Santos zu werden!",
          "contact": "santos@email.de",
          "lat": "50.92565800",
          "lng": "6.95598000",
          "files": []
        }
      ],
      "projectsSupported": [],
      "files": []
    },
    {
      "id": 4,
      "name": "Clint Lockman",
      "email": "jules34@example.com",
      "first_name": "Ashly",
      "last_name": "Fadel",
      "contact": null,
      "helper": {
        "address": {
          "id": 4,
          "addressable_id": 4,
          "addressable_type": "App\\Helper",
          "street": "Konopelski Turnpike",
          "street_number": "Falls",
          "zip": "22173",
          "city": "New Christina",
          "lat": "-73.45232400",
          "lng": "3.47635500",
          "created_at": "2020-03-30T15:27:31.000000Z",
          "updated_at": "2020-03-30T15:27:31.000000Z",
          "deleted_at": null
        }
      },
      "company": null,
      "projectsOwned": [],
      "projectsSupported": [],
      "files": []
    },
    {
      "id": 5,
      "name": "Howell VonRueden",
      "email": "allen19@example.net",
      "first_name": "Bridget",
      "last_name": "Schiller",
      "contact": null,
      "helper": {
        "address": {
          "id": 5,
          "addressable_id": 5,
          "addressable_type": "App\\Helper",
          "street": "Morar Brook",
          "street_number": "Wells",
          "zip": "56751",
          "city": "North Florencioport",
          "lat": "-29.11525000",
          "lng": "-33.48731400",
          "created_at": "2020-03-30T15:27:31.000000Z",
          "updated_at": "2020-03-30T15:27:31.000000Z",
          "deleted_at": null
        }
      },
      "company": null,
      "projectsOwned": [],
      "projectsSupported": [],
      "files": []
    },
    {
      "id": 6,
      "name": "Gilberto Leannon",
      "email": "btromp@example.com",
      "first_name": "Libbie",
      "last_name": "Mayer",
      "contact": null,
      "helper": {
        "address": {
          "id": 6,
          "addressable_id": 6,
          "addressable_type": "App\\Helper",
          "street": "Trantow Road",
          "street_number": "Underpass",
          "zip": "93471",
          "city": "Lake Giovanni",
          "lat": "-56.66846100",
          "lng": "-46.58333900",
          "created_at": "2020-03-30T15:27:31.000000Z",
          "updated_at": "2020-03-30T15:27:31.000000Z",
          "deleted_at": null
        }
      },
      "company": null,
      "projectsOwned": [],
      "projectsSupported": [],
      "files": []
    },
    {
      "id": 7,
      "name": "Gretchen Dare",
      "email": "cornell28@example.org",
      "first_name": "Virgil",
      "last_name": "Jaskolski",
      "contact": null,
      "helper": {
        "address": {
          "id": 7,
          "addressable_id": 7,
          "addressable_type": "App\\Helper",
          "street": "Melvin Track",
          "street_number": "Freeway",
          "zip": "42306-0299",
          "city": "Port Arnoldo",
          "lat": "61.79164900",
          "lng": "76.37078500",
          "created_at": "2020-03-30T15:27:31.000000Z",
          "updated_at": "2020-03-30T15:27:31.000000Z",
          "deleted_at": null
        }
      },
      "company": null,
      "projectsOwned": [],
      "projectsSupported": [],
      "files": []
    },
    {
      "id": 8,
      "name": "Brice Gorczany Jr.",
      "email": "amurazik@example.com",
      "first_name": "Peter",
      "last_name": "Yost",
      "contact": null,
      "helper": {
        "address": {
          "id": 8,
          "addressable_id": 8,
          "addressable_type": "App\\Helper",
          "street": "Mariano Squares",
          "street_number": "Lake",
          "zip": "74003",
          "city": "Lake Damaris",
          "lat": "50.30977100",
          "lng": "48.34507300",
          "created_at": "2020-03-30T15:27:31.000000Z",
          "updated_at": "2020-03-30T15:27:31.000000Z",
          "deleted_at": null
        }
      },
      "company": null,
      "projectsOwned": [],
      "projectsSupported": [],
      "files": []
    },
    {
      "id": 9,
      "name": "Dr. Mavis Weimann",
      "email": "maxine36@example.net",
      "first_name": "Eric",
      "last_name": "Fay",
      "contact": null,
      "helper": {
        "address": {
          "id": 9,
          "addressable_id": 9,
          "addressable_type": "App\\Helper",
          "street": "Roberts Plaza",
          "street_number": "Parks",
          "zip": "23327-5269",
          "city": "West Kathleen",
          "lat": "75.92232300",
          "lng": "143.76247000",
          "created_at": "2020-03-30T15:27:31.000000Z",
          "updated_at": "2020-03-30T15:27:31.000000Z",
          "deleted_at": null
        }
      },
      "company": null,
      "projectsOwned": [],
      "projectsSupported": [],
      "files": []
    },
    {
      "id": 10,
      "name": "Mr. Elwin Macejkovic",
      "email": "zsimonis@example.org",
      "first_name": "Joanne",
      "last_name": "Hauck",
      "contact": null,
      "helper": {
        "address": {
          "id": 10,
          "addressable_id": 10,
          "addressable_type": "App\\Helper",
          "street": "Hill Curve",
          "street_number": "Ville",
          "zip": "02944",
          "city": "South Doug",
          "lat": "14.98676600",
          "lng": "-151.87494300",
          "created_at": "2020-03-30T15:27:31.000000Z",
          "updated_at": "2020-03-30T15:27:31.000000Z",
          "deleted_at": null
        }
      },
      "company": null,
      "projectsOwned": [],
      "projectsSupported": [],
      "files": []
    },
    {
      "id": 11,
      "name": "Rosie Robel",
      "email": "helene.feest@example.org",
      "first_name": "Lukas",
      "last_name": "Kuhic",
      "contact": null,
      "helper": null,
      "company": {
        "established_at": "1944-08-17T00:00:00.000000Z",
        "name": "Rohan-Feeney",
        "address": {
          "id": 11,
          "addressable_id": 1,
          "addressable_type": "App\\Company",
          "street": "Adams Village",
          "street_number": "Ports",
          "zip": "49677",
          "city": "Hudsonview",
          "lat": "25.38681800",
          "lng": "40.57233600",
          "created_at": "2020-03-30T15:27:31.000000Z",
          "updated_at": "2020-03-30T15:27:31.000000Z",
          "deleted_at": null
        }
      },
      "projectsOwned": [],
      "projectsSupported": [],
      "files": []
    },
    {
      "id": 12,
      "name": "Kameron Monahan",
      "email": "jared.nikolaus@example.net",
      "first_name": "Orion",
      "last_name": "Towne",
      "contact": null,
      "helper": null,
      "company": {
        "established_at": "1983-11-30T00:00:00.000000Z",
        "name": "Romaguera-Howe",
        "address": {
          "id": 12,
          "addressable_id": 2,
          "addressable_type": "App\\Company",
          "street": "Jacobi Passage",
          "street_number": "Park",
          "zip": "92434-1702",
          "city": "East Arch",
          "lat": "2.27332300",
          "lng": "-21.54185100",
          "created_at": "2020-03-30T15:27:31.000000Z",
          "updated_at": "2020-03-30T15:27:31.000000Z",
          "deleted_at": null
        }
      },
      "projectsOwned": [],
      "projectsSupported": [],
      "files": []
    },
    {
      "id": 13,
      "name": "Madyson Nolan PhD",
      "email": "shyanne90@example.org",
      "first_name": "Roberta",
      "last_name": "Price",
      "contact": null,
      "helper": null,
      "company": {
        "established_at": "1987-03-23T00:00:00.000000Z",
        "name": "Quigley and Sons",
        "address": {
          "id": 13,
          "addressable_id": 3,
          "addressable_type": "App\\Company",
          "street": "Runte Points",
          "street_number": "Throughway",
          "zip": "76788",
          "city": "West Noemybury",
          "lat": "-48.96515300",
          "lng": "171.80064200",
          "created_at": "2020-03-30T15:27:31.000000Z",
          "updated_at": "2020-03-30T15:27:31.000000Z",
          "deleted_at": null
        }
      },
      "projectsOwned": [],
      "projectsSupported": [],
      "files": []
    },
    {
      "id": 14,
      "name": "Gennaro Barrows III",
      "email": "madyson62@example.org",
      "first_name": "Nicholas",
      "last_name": "Mertz",
      "contact": null,
      "helper": null,
      "company": {
        "established_at": "1995-08-07T00:00:00.000000Z",
        "name": "Kuhlman Inc",
        "address": {
          "id": 14,
          "addressable_id": 4,
          "addressable_type": "App\\Company",
          "street": "Waelchi Causeway",
          "street_number": "Rapids",
          "zip": "26394",
          "city": "Kundeland",
          "lat": "86.36232000",
          "lng": "143.87040200",
          "created_at": "2020-03-30T15:27:31.000000Z",
          "updated_at": "2020-03-30T15:27:31.000000Z",
          "deleted_at": null
        }
      },
      "projectsOwned": [],
      "projectsSupported": [],
      "files": []
    },
    {
      "id": 15,
      "name": "Ben Bahringer",
      "email": "hane.marcia@example.org",
      "first_name": "Rosina",
      "last_name": "Carter",
      "contact": null,
      "helper": null,
      "company": {
        "established_at": "1971-12-08T00:00:00.000000Z",
        "name": "Reichert, Krajcik and Waters",
        "address": {
          "id": 15,
          "addressable_id": 5,
          "addressable_type": "App\\Company",
          "street": "Ed Port",
          "street_number": "Run",
          "zip": "31352-5557",
          "city": "Sigridberg",
          "lat": "61.61760600",
          "lng": "104.54125800",
          "created_at": "2020-03-30T15:27:31.000000Z",
          "updated_at": "2020-03-30T15:27:31.000000Z",
          "deleted_at": null
        }
      },
      "projectsOwned": [],
      "projectsSupported": [],
      "files": []
    }
  ],
  "links": {
    "first": "https:\/\/weyounite.dev\/api\/users?page=1",
    "last": "https:\/\/weyounite.dev\/api\/users?page=2",
    "prev": null,
    "next": "https:\/\/weyounite.dev\/api\/users?page=2"
  },
  "meta": {
    "current_page": 1,
    "from": 1,
    "last_page": 2,
    "path": "https:\/\/weyounite.dev\/api\/users",
    "per_page": 15,
    "to": 15,
    "total": 21
  }
}
```

## GET /api/users/{id}

### response

```json
{
  "data": {
    "id": 1,
    "name": "Paola",
    "email": "paola_hair@email.de",
    "email_verified_at": "2020-03-22T19:08:35.000000Z",
    "contact": "+49\/17610010010",
    "lat": "50.93556800",
    "lng": "6.92018900",
    "created_at": null,
    "updated_at": null,
    "deleted_at": null
  }
}
```

## PUT /api/users/{id}

## DELETE /api/users/{id}

### response

```json
{
  "sucess": true
}
```

## POST /api/users/upload/{1}

upload a file with attribute 'file'

### response

```json
{
  "sucess": true
}
```

# projects_supported

## GET /api/projects_supported

### response

```json
{
  "current_page": 1,
  "data": [
    {
      "id": 1,
      "user_id": 2,
      "project_id": 2,
      "created_at": "2020-03-22T10:37:11.000000Z",
      "updated_at": "2020-03-22T11:43:35.000000Z"
    }
  ],
  "first_page_url": "https:\/\/weyounite.dev\/api\/projects_supported?page=1",
  "from": 1,
  "last_page": 1,
  "last_page_url": "https:\/\/weyounite.dev\/api\/projects_supported?page=1",
  "next_page_url": null,
  "path": "https:\/\/weyounite.dev\/api\/projects_supported",
  "per_page": 15,
  "prev_page_url": null,
  "to": 1,
  "total": 1
}
```

## POST /api/projects_supported

### body

```json
{
  "data": {
    "user_id": "{user_id}",
    "project_id": "{project_id}" 
  }
}
```

## PUT /api/projects_supported/{id}

## DELETE /api/projects_supported/{id}

### response

```json
{
  "sucess": true
}
```

# messages

## POST /api/messages/send/{receiverID}  (send direct message to an user)

### body 

```json
{
  "message": "the text you want to send",
  "replyToId": "can be nullable or the ID of a message you want to reply to"
}
```

## POST /api/messages/mark_as_read/{receiverID}  (send direct message to an user)

## GET /api/messages/{messageID}

### response

```json
{
  "data": {
    "id": "a707bd67-309d-4bf0-bb7a-d6d595ba2b18",
    "senderId": 1,
    "replyToId": null,
    "message": "ich bin jetzt verfuegbar",
    "created_at": "2020-03-27T18:51:21.000000Z",
    "read_at": "2020-03-27T19:11:08.000000Z"
  }
}
```

## GET /api/messages/

### response
```json
{
  "data": [
    {
      "data": {
        "id": "afc6e235-9f47-4f23-898a-cce2d1ac6487",
        "senderId": 1,
        "replyToId": "a707bd67-309d-4bf0-bb7a-d6d595ba2b18",
        "message": "ich bin jetzt verfuegbar",
        "created_at": "2020-03-27T18:53:07.000000Z",
        "read_at": null
      }
    },
    {
      "data": {
        "id": "a707bd67-309d-4bf0-bb7a-d6d595ba2b18",
        "senderId": 1,
        "replyToId": null,
        "message": "ich bin jetzt verfuegbar",
        "created_at": "2020-03-27T18:51:21.000000Z",
        "read_at": "2020-03-27T19:11:08.000000Z"
      }
    }
  ],
  "links": {
    "first": "https:\/\/weyounite.dev\/api\/messages?page=1",
    "last": "https:\/\/weyounite.dev\/api\/messages?page=1",
    "prev": null,
    "next": null
  },
  "meta": {
    "current_page": 1,
    "from": 1,
    "last_page": 1,
    "path": "https:\/\/weyounite.dev\/api\/messages",
    "per_page": 15,
    "to": 2,
    "total": 2
  }
}
```

## DELETE /api/messages/{messageID}
