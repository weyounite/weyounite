# create a new user

```
php artisan nova:user
```

# manage allowed users for Nova

I created a config file named 'settings'. There is an option array where allowed users can be listed.

```php
return [
    'nova' => [
        'allowedUsers' => [
            //type allowed user emails here
        ]
    ]
];
```
