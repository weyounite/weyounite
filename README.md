# Inspiration
Wir möchten Unternehmen in dieser Krise unterstützen - aber auch darüber hinaus! Einkaufsstraßen sind menschenleer, Metropolen gleichen Geisterstädten und der Inhaber vom kleinen Friseurladen von Nebenan ist leider ohne Kunden. Doch zeitgleich können Fitnesstrainer Ihre Kurse online geben, Tanzlehrer vor einer Leinwand im leeren Studio unterrichten. Not macht erfinderisch - viele Unternehmer suchen neue Wege, um trotz Corona und anderen Marktveränderungen im Business zu bleiben. Unternehmen fehlt es neben Umsatz auch an Ideen und Wissen, wie sie diese Herausforderung meistern sollen.

Wir möchten die Unternehmen dabei unterstützen: erfinderisch zu werden und zu bleiben, aber auch Ihnen die Umsetzung zu ermöglichen. Das erreichen wir durch finanzielle Hilfe über den Verkauf von Vouchers, den Empfang von Spenden, oder den Austausch von Know-How und Fertigkeiten.

# What it does
"Können wir das schaffen - Jo, wir schaffen das". Bob der Baumeister und Obama waren mit diesem Ansatz schon erfolgreich. We.Younite bringt Leute zusammen: You.nitees (Unternehmer), die vor Herausforderungen stehen und You.niter (Helfern), die sie dabei unterstützen wollen. Beide registrieren sich auf We.Younite. Über die Suchfunktion kann man You.nitees in seinem Umkreis und darüber heraus finden, die Hilfe benötigen. Die Herausforderungen der lokalen Unternehmen sind vielfältig und demzufolge bieten wir auch unterschiedliche Unterstützungsmöglichkeiten gesammelt auf einer Platform:

You.niters können direkt einen Betrag Ihrer Wahl spenden,

You.niters können einen Gutschein für das Geschäft kaufen um kurzfristig Liquidität zu ermöglichen, oder

mit ihrem Know-how, Wissen und den eigenen beiden Händen den You.nitees helfen.

Wenn für einen selber keine passende „Herausforderung“ dabei ist hat jeder You.niter die Möglichkeit die Profile der Unternehmen an Freunde weiterzuleiten und zu empfehlen. Auch Stammkunden können ihre Lieblingsgeschäfte auf We.Younite empfehlen, um möglichst viele passende You.niter zu finden.

Unser Ziel ist ein Happy End: Jeder You.nitee soll seine passenden You.niter finden, um gemeinsam etwas herausragendes zu schaffen.

It's not I, not you: It's We - We.Younite!

# How we built it
In unserem Projekt handelt es sich um eine Webbasierte Plattform.

Um eine nahezu unbegrenzte Skalierung in die Breite zu ermöglichen haben wir uns von Anfang an für Enterprise Technologien entschieden. Unser gesamtes Backend läuft über Laravel welche stateless in Container in einer AWS oder Azure betrieben werden soll. Durch den stateless Betrieb können wir sicherstellen, dass zu Lastspitzen unbegrenzt viele extra Container bereitgestellt werden können und wir mit hohem Traffic Aufkommen zurecht kommen werden.

# Tech setup

Bei den Datenbank Technologien setzen wir aktuell auf MySQL und in Zukunft auch auf Elasticsearch. Beide Technologien werden von Cloud Providern wie AWS und Azure unterstützt und sind nahezu unbegrenzt skalierbar durch Active-Active Cluster Technologien. Elasticsearch bietet hier den Vorteil, dass bei Volltextsuchen selbst in mehreren Terrabyte großen Datenbeständen Antwortzeiten im Millisekunden-Bereich möglich sind.

Im Frontend setzten wir auf VueJS welches eine komplett im Client-seitig betriebene Anwendung ermöglicht, sodass alle Computing-Ressourcen im Backend nur für eigentliche Business Verarbeitung genutzt wird. VueJS wird über ein global gespanntes CDN in der Cloud wie S3 ausgeliefert, um performante Ladezeiten für den Nutzer zu ermöglichen.

Durch die komplette Entkopplung von Frontend und Backend durch VueJS und Laravel können wir sehr einfach auch auf andere Frontend Technologien gehen wie z.B. mobile Apps. Hier können wir ohne Backend Anpassungen Electron oder Flotter einführen, um dem User auch eine native mobile App bereitzustellen.

#### Laravel Nova Lizenz

Das Projekt nutzt Laravel Nova. Eine Pro Lizenz hierfür wurde mit freundlicher Unterstützung von [Publicare Marketing Communications GmbH](https://www.publicare.de) zur Verfügung gestellt.
